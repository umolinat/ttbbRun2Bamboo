#!/usr/bin/env python

import argparse
import json
import os
import random
import itertools
import numpy as np

import ROOT as R
R.gROOT.SetBatch(True)

import HistogramTools as HT

def optimizeBinning(fineHist, maxUncertainty, acceptLargerOverFlowUncert=True):
    """ Optimize binning, return result in the form of:
        (list of edges, list of bin numbers of original histo)
        args:
            - acceptLargerOFlowUncert: if false, will always merge overflow with previous bin
                                        to ensure the overflow uncertainty is below the threshold
    """
    
    # Get projection on reco axis
    recoProjection = fineHist.ProjectionY(fineHist.GetName() + "reco_projection")

    # Find first bin with non-zero content
    startBin = 1
    fineNBins = recoProjection.GetNbinsX()
    for i in range(1, fineNBins + 1):
        if recoProjection.GetBinContent(i) == 0:
            startBin += 1
        else:
            break

    print(f"New start bin: {startBin}")

    binContents = list(recoProjection)
    binSumW2 = list(recoProjection.GetSumw2())
    finalBins = [startBin] # array [i j k ...] -> bin 1 in new histo = bins i ... j-1 in fine one, etc.

    upEdge = startBin
    mergeLastBin = False
    while upEdge <= fineNBins + 1:
        uncertainty = 99999
        content = 0
        sumw2 = 0
        while uncertainty > maxUncertainty:
            if upEdge == fineNBins + 2:
                # we've now included the overflow bin without going below the required uncertainty
                # -> stop and make sure we merge last bin with next-to-last one
                mergeLastBin = True
                break
            content += binContents[upEdge]
            sumw2 += binSumW2[upEdge]
            if content != 0:
                uncertainty = np.sqrt(sumw2) / content
            upEdge += 1
        finalBins.append(upEdge)
    
    if mergeLastBin and acceptLargerOverFlowUncert:
        del finalBins[-2]

    # We now have the new binning. Find the lower edges it corresponds to.
    # The last bin will correspond to the overflow bin so we don't include it explicitly in the edges
    newEdges = []
    for i in finalBins[:-1]:
        newEdges.append(recoProjection.GetXaxis().GetBinLowEdge(i))

    print("New binning:")
    print(newEdges)
    print(finalBins)
    
    return newEdges, finalBins


def rebinCustom(hist, binning, name):
    """ Rebin 2D hist using bin numbers and edges as returned by optimizeBinning() """

    edges = binning[0]
    #print("edges")
    #print(edges)
    nBins = len(edges) - 1
    oldNbins = hist.GetNbinsX()
    newHist = R.TH2D(name, hist.GetTitle(), nBins, np.array(edges), nBins, np.array(edges))
    newHist.Sumw2()
    oldSumw2 = list(hist.GetSumw2())

    # we fill the overflow bins properly but not the underflows
    for newBinX in range(1, nBins + 2):
        for newBinY in range(1, nBins + 2):
            content = 0
            sumw2 = 0
            maxOldBinX = binning[1][newBinX] if newBinX <= nBins else oldNbins + 2
            for oldBinX in range(binning[1][newBinX - 1], maxOldBinX):
                maxOldBinY = binning[1][newBinY] if newBinY <= nBins else oldNbins + 2
                for oldBinY in range(binning[1][newBinY - 1], maxOldBinY):
                    content += hist.GetBinContent(oldBinX, oldBinY)
                    sumw2 += oldSumw2[hist.GetBin(oldBinX, oldBinY)]
            newHist.SetBinContent(newBinX, newBinY, content)
            newHist.GetSumw2()[newHist.GetBin(newBinX, newBinY)] = sumw2

    return newHist

def plotSlices(hist, name, folder, norm=True):
    """ Plot gen-slices from 2D reco vs. gen hist """

    style = HT.setTDRStyle()
    style.SetMarkerStyle(0)

    c = R.TCanvas("c", "c")

    colors = itertools.cycle([
        "#7044f1",
        "#45ad95",
        "#c07f00",
        "#da7fb7",
        "#ff2719",
        "#251c00"]
    )

    nBins = hist.GetNbinsX()
    projs = []
    legEntries = []
    # get all projections in the reco axis including the gen overflow bin
    for xBin in range(1, nBins + 2):
        projs.append(hist.ProjectionY(name + str(xBin), xBin, xBin))
        upEdge = hist.GetXaxis().GetBinLowEdge(xBin+1) if xBin <= nBins else -1
        legEntries.append(f"Gen {name} from {hist.GetXaxis().GetBinLowEdge(xBin):.2f} to {upEdge:.2f}")

    projs[0].SetLineWidth(2)
    projs[0].SetLineStyle(2)
    projs[0].SetLineColor(R.TColor.GetColor(next(colors)))
    projs[0].GetYaxis().SetLabelSize(0.03)
    projs[0].GetYaxis().SetTitleSize(0.03)
    projs[0].GetYaxis().SetTitleOffset(1.7)
    # projs[0].GetYaxis().SetTitle("Events")
    projs[0].GetXaxis().SetTitle("Reco " + name)
    projs[0].GetXaxis().SetLabelSize(0.03)
    projs[0].GetXaxis().SetTitleSize(0.03)
    # projs[0].GetXaxis().SetLabelOffset(0.05)
    projs[0].GetXaxis().SetTitleOffset(1.5)
    if norm:
        # include the overflow in the integrals
        projs[0].Scale(1./projs[0].Integral(1, nBins + 1))
    projs[0].Draw("Lhist")
    projs[0].Draw("E0same")

    for i, proj in enumerate(projs[1:]):
        proj.SetLineWidth(2)
        proj.SetLineColor(R.TColor.GetColor(next(colors)))
        proj.SetLineStyle(2)
        if norm:
            proj.Scale(1./proj.Integral(1, nBins + 1))
        proj.Draw("Lhistsame")
        proj.Draw("E0same")
        # make sure we also draw the overflow bin
        proj.GetXaxis().SetRange(1, nBins + 1)
    
    histMax = -100
    histMin = 9999999
    for i in range(1, projs[0].GetNbinsX() + 2):
        histMax = max(histMax, *[h.GetBinContent(i) for h in projs])
        histMin = min(histMin, *[h.GetBinContent(i) for h in projs])
    projs[0].GetYaxis().SetRangeUser(0, histMax * 1.6)
    projs[0].GetXaxis().SetRange(1, nBins + 1)

    l = R.TLegend(0.53, 0.65, 0.98, 0.92)
    l.SetTextFont(42)
    l.SetFillColor(R.kWhite)
    l.SetFillStyle(0)
    l.SetBorderSize(0)

    for i in range(len(projs)):
        l.AddEntry(projs[i], legEntries[i])
    l.Draw("same")

    c.SaveAs(os.path.join(folder, name + ".pdf"))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='input file', required=True)
    parser.add_argument('-o', '--output', help='json output file name for new binning')
    parser.add_argument('-f', '--folder', help='output folder for slice plots', required=True)
    parser.add_argument('-u', '--uncertainty', type=float, default=0.05, help='max stat. uncertainty')
    parser.add_argument('-c', '--compute', action='store_true', help='compute new binning and rebin 2D histograms before plotting them')
    args = parser.parse_args()
    eras = ["2016", "2017", "2018", "run2"]
    
    for year in eras:
        #Temporary if for the 4f
        if year != "2017":
            if not os.path.isdir(args.folder+"_"+year+"_slice_plots"):
                os.makedirs(args.folder+"_"+year+"_slice_plots")
            inFile = HT.openFileAndGet(args.input+"_"+year+".root")
            outFile = HT.openFileAndGet(os.path.join(args.folder+"_"+year+"_slice_plots", "rebinned.root"), "recreate")

            nameTemplate = "1lep_6j_4b_extra_jet_{}_comparison"
            observables = ["pTbb", "Mbb", "DRbb"]

            binnings = {}
            for obs in observables:
                print(f"Doing {obs}")
                fineHist = inFile.Get(nameTemplate.format(obs))
                if args.compute:
                    binning = optimizeBinning(fineHist, args.uncertainty)
                    binnings[obs] = binning[0]
                    newHist = rebinCustom(fineHist, binning, fineHist.GetName() + "_rebin")
                else:
                    newHist = fineHist
                outFile.cd()
                print(np.sqrt(sum(list(newHist.GetSumw2())))/newHist.Integral())
                newHist.Write()
                plotSlices(newHist, obs, args.folder+"_"+year+"_slice_plots")

            if args.output is not None and args.compute:
                with open(args.output+"_"+year+"_bin.json", 'w') as _f:
                    json.dump(binnings, _f)
            inFile.Close()
            outFile.Close()
