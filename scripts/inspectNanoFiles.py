#!/usr/bin/env python

import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)

import argparse
import yaml
import json

from utils import PFN_PREFIX

def analyzeSample(sample):
    path = PFN_PREFIX + sample["file_infos"][0]["lfn"]
    tf = ROOT.TFile.Open(path)
    tt = tf.Get("Events")
    tt.GetEntry(0)

    issue = False
    msg = f"==== {sample['request_name']} ====\n"

    isTopNano = hasattr(tt, "GenJet_nBHadFromT")
    if isTopNano:
        msg += "- Is topNanoAOD\n"

    # LHE scale weights
    expNScaleWeights = 9
    if tt.nLHEScaleWeight != expNScaleWeights:
        issue = True
        msg += f"- nLHEScaleWeights = {tt.nLHEScaleWeight} instead of {expNScaleWeights} -- weights for first event are {[i for i in tt.LHEScaleWeight]}\n"
    # expScaleWeightDocStr = "LHE scale variation weights (w_var / w_nominal); [0] is renscfact=0.5d0 facscfact=0.5d0 ; [1] is renscfact=0.5d0 facscfact=1d0 ; [2] is renscfact=0.5d0 facscfact=2d0 ; [3] is renscfact=1d0 facscfact=0.5d0 ; [4] is renscfact=1d0 facscfact=1d0 ; [5] is renscfact=1d0 facscfact=2d0 ; [6] is renscfact=2d0 facscfact=0.5d0 ; [7] is renscfact=2d0 facscfact=1d0 ; [8] is renscfact=2d0 facscfact=2d0 "
    if tt.nLHEScaleWeight > 4 and abs(tt.LHEScaleWeight[4] - 1) > 1e-4:
        issue = True
        msg += f"- LHEScaleWeight[4] for first event is too different from 1: {tt.LHEScaleWeight[4]}\n"
    msg += "- LHE scale weights title: "
    msg += f"'{tt.GetBranch('nLHEScaleWeight').GetTitle()}'\n"

    # PS weights
    if tt.nPSWeight == 46:
        msg += "- Has all 46 PS weights\n"
    elif tt.nPSWeight == 14:
        msg += "- Has 2+12 def/con/red PS weights\n"
    elif tt.nPSWeight == 4:
        msg += "- Has regular 4 PS weights\n"
    if isTopNano:
        if tt.nPSWeight != 46 and tt.nPSWeight != 14:
            issue = True
            msg += f"- nPSWeight = {tt.nPSWeight}, for topNanoAOD expected either 14 or 46\n"
    else:
        if tt.nPSWeight != 4:
            issue = True
            msg += f"- nPSWeight = {tt.nPSWeight} instead of 4\n"

    msg += "\n"

    return issue,msg

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gather the information on a processed sample and put that information in a JSON file')
    parser.add_argument('JSON', type=str, nargs='+', metavar='JSON',
                        help='JSON files to process, produced by runPostCrab.py')
    parser.add_argument('-e', '--eras', nargs='*', help='Restrict to given era(s)')
    options = parser.parse_args()

    noIssue,withIssue = dict(),dict()

    # Load all JSON files
    for path in options.JSON:
        with open(path) as _f:
            sample = json.load(_f)
            # skipping data since we're only checking the event weights (for now...)
            if any(d in sample["request_name"] for d in ["EGamma", "SingleMuon", "SingleElectron"]):
                continue
            if options.eras:
                keep = False
                for era in options.eras:
                    if sample["request_name"].endswith(era):
                        keep = True
                        break
                if not keep:
                    continue
            issue,msg = analyzeSample(sample)
            era = sample["request_name"][-4:]
            if issue:
                withIssue.setdefault(era, "")
                withIssue[era] += msg
            else:
                noIssue.setdefault(era, "")
                noIssue[era] += msg

    print("Samples with issues:\n")
    for era in sorted(withIssue.keys()):
        print(f"== ERA: {era} ==\n")
        print(withIssue[era])

    print("Samples without issues:\n")
    for era in sorted(noIssue.keys()):
        print(f"== ERA: {era} ==\n")
        print(noIssue[era])



