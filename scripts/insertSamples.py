#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division

import argparse
import yaml
import logging
logging.basicConfig(level=logging.DEBUG)

from utils import insertSampleFilesIntoTemplate

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Gather the information on a processed sample and put that information in a JSON file')
    parser.add_argument('JSON', type=str, nargs='+', metavar='JSON',
                        help='JSON files to process, produced by runPostCrab.py')
    parser.add_argument('-i', '--input', type=str, default='.', metavar='YML',
                        help='Input yml file with sample settings, acting as template')
    parser.add_argument('-o', '--output', type=str, default='.', metavar='YML',
                        help='Output yml file with the full information about all samples')
    parser.add_argument('-e', '--eras', nargs='*', help='Restrict to given era(s)')
    options = parser.parse_args()

    # Load yml template
    with open(options.input) as _f:
        template = yaml.load(_f, Loader=yaml.SafeLoader)
    
    outTemplate = insertSampleFilesIntoTemplate(template, options.JSON, options.eras)

    # Write complete yml config
    with open(options.output, 'w') as _f:
        yaml.dump(outTemplate, _f)
