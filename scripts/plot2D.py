#!/usr/bin/env python

import argparse
import os
import yaml

import HistogramTools as HT


"""
Plot 2D histograms for data, MC, and ratio data/MC. Hardcoded configuration at the moment.,.
"""

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', default='./', help='input folder')
parser.add_argument('-c', '--config', help='plot yml file')
parser.add_argument('-o', '--output', help='output folder')

args = parser.parse_args()

import ROOT as R
R.gROOT.SetBatch(True)

style = HT.setTDRStyle()
style.SetPadRightMargin(0.15)

runs = {
      "Run2018A" : 14027.614
    , "Run2018B" : 7066.552
    , "Run2018C" : 6898.817
    , "Run2018D" : 31747.582
}

hists = {
    '1ele_4j_2b_jet_eta_vs_phi': { 'legX': 'Leading jet eta', 'legY': 'Leading jet phi', 'rebin': 1, 'channel': '1 electron, #geq 4 jets, #geq 2 b tags' },
    '1mu_4j_2b_jet_eta_vs_phi': { 'legX': 'Leading jet eta', 'legY': 'Leading jet phi', 'rebin': 1, 'channel': '1 muon, #geq 4 jets, #geq 2 b tags' },
    '1ele_4j_2b_lep_eta_vs_phi': { 'legX': 'Lepton eta', 'legY': 'Lepton phi', 'rebin': 1, 'channel': '1 electron, #geq 4 jets, #geq 2 b tags' },
    '1mu_4j_2b_lep_eta_vs_phi': { 'legX': 'Lepton eta', 'legY': 'Lepton phi', 'rebin': 1, 'channel': '1 muon, #geq 4 jets, #geq 2 b tags' },
}

data = {
    'Run2018A': ['EGamma_Run2018A-Nano1June2019-v1.root', 'SingleMuon_Run2018A-Nano1June2019-v1.root'],
    'Run2018B': ['EGamma_Run2018B-Nano1June2019-v1.root', 'SingleMuon_Run2018B-Nano1June2019-v1.root'],
    'Run2018C': ['EGamma_Run2018C-Nano1June2019-v1.root', 'SingleMuon_Run2018C-Nano1June2019-v1.root'],
    'Run2018D': ['EGamma_Run2018D-Nano1June2019-v1.root', 'SingleMuon_Run2018D-Nano1June2019-v1.root'],
}


# myRatioPalette = HT.RatioPalette([("temperatureMap", False)])
myRatioPalette = HT.RatioPalette([("blackBody", True)])

def plot(hist, title, postfix="", label="", legX="", legY="", channel="", ratio=False, rebin=None):
    c = R.TCanvas(title, title, 800, 600)
    if rebin:
        hist.Rebin2D(rebin, rebin)
    if ratio:
        myRatioPalette.set(hist, middle=0.36)
    hist.Draw("colz0")
    hist.SetXTitle(legX)
    hist.SetYTitle(legY)
    text = R.TLatex(0.16, 0.96, label)
    text.SetNDC(True)
    text.SetTextFont(42)
    text.SetTextSize(0.03)
    text.Draw("same")
    textChannel = R.TLatex(0.56, 0.96, channel)
    textChannel.SetNDC(True)
    textChannel.SetTextFont(42)
    textChannel.SetTextSize(0.03)
    textChannel.Draw("same")
    c.Print(os.path.join(args.output, title + postfix + '.pdf'))
    if ratio:
        myRatioPalette.reset()


def get(path, hist):
    _f = HT.openFileAndGet(path)
    hist = _f.Get(hist)
    hist.SetDirectory(0)
    return hist

with open(args.config) as _f:
    ymlCfg = yaml.load(_f)

mc = []
for name, proc in ymlCfg['files'].items():
    if proc['type'] == 'data':
        continue
    mc.append((name, proc['cross-section'], proc['generated-events']))

for histTitle, options in hists.items():
    # plot MC sum
    mcHist = get(os.path.join(args.input, mc[0][0]), histTitle).Clone("MC sum")
    mcHist.Scale(mc[0][1] / mc[0][2])
    for proc in mc[1:]:
        mcHist.Add(get(os.path.join(args.input, proc[0]), histTitle), proc[1] / proc[2])
    plot(mcHist, histTitle, postfix='_MCsum', label="MC sum", **options)

    # plot data run per run
    dataSum = get(os.path.join(args.input, list(data.values())[0][0]), histTitle).Clone("Data sum")
    dataSum.Reset()
    for run, dataFiles in data.items():
        dataHist = get(os.path.join(args.input, dataFiles[0]), histTitle).Clone("Data {}".format(run))
        for _f in dataFiles[1:]:
            dataHist.Add(get(os.path.join(args.input, _f), histTitle))
        dataSum.Add(dataHist)
        plot(dataHist, histTitle, postfix='_data' + run, label="Data " + run, **options)
        mcRun = mcHist.Clone("MC sum run " + run)
        mcRun.Scale(runs[run])
        dataHist.Divide(mcRun)
        # plot data/MC for current run
        plot(dataHist, histTitle, postfix='_ratio' + run, label="Ratio data " + run + "/MC", ratio=True, **options)
    
    # data sum
    plot(dataSum, histTitle, postfix='_dataSum', label="Data sum", **options)

    # data/MC everything
    mcHist.Scale(sum(runs.values()))
    dataSum.Divide(mcHist)
    plot(dataSum, histTitle, postfix='_ratioSum', label="Ratio data/MC", ratio=True, **options)

