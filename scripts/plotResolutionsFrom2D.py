#!/usr/bin/env python

import argparse
import os
import HistogramTools as HT
import ROOT as R
R.gROOT.SetBatch(True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', nargs='+', help='input file(s)')
    parser.add_argument('-f', '--folder', default='.', help='output folder')
    args = parser.parse_args()

    if not os.path.exists(args.folder):
        os.makedirs(args.folder)

    for inPath in args.input:

        inFile = HT.openFileAndGet(inPath)
        outPath = os.path.join(args.folder, os.path.basename(inPath))

        if os.path.exists(outPath):
            print(f"Warning: {outPath} already exist, will not overwrite - skipping!")
            continue
        print(f"Writing produced histograms to {outPath}")
        outFile = R.TFile(outPath, "recreate")

        observables = ["pTbb", "Mbb", "DRbb"]
        categories = ["mu", "lep", "ele"]

        for obs in observables:
            for cat in categories:
                # Define histogram to be analyzed
                histogram = inFile.Get('1%s_6j_4b_extra_jet_%s_comparison' % (cat, obs))

                # Rebin the original histogram
                histogram.Rebin2D(5,5)

                # Do the fit for all vertical slices
                resHistogram = R.TObjArray()
                histogram.FitSlicesX(0, 0, -1, 0, "", resHistogram)

                # Write histograms and pdfs
                canvHistogram = R.TCanvas()
                resHistogram[2].Draw()
                resHistogram[2].Write()

                canvHistogram.SaveAs(outPath.split('.root')[0] + '_%s_%s.pdf' % (cat, obs))

        outPath.Close()

    print("Done")
