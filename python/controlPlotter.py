import os
import json
import yaml
import glob
import shutil

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.analysisutils import parseAnalysisConfig
from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo.plots import CutFlowReport
from bamboo import treefunctions as op

import definitions as defs
import utils
import controlPlotDefinition as cp
from recoBaseTtbbPlotter import recoBaseTtbbPlotter

class controlPlotter(recoBaseTtbbPlotter):
    """"""
    def __init__(self, args):
        super(controlPlotter, self).__init__(args)

    def definePlots(s, t, noSel, sample=None, sampleCfg=None):
        super().defineObjects(t, noSel, sample, sampleCfg)
        era = sampleCfg["era"]
        plots = []

        ##### Muon and electron selection, exclusive!
        oneMuTriggerSel, oneEleTriggerSel = recoBaseTtbbPlotter.defineBaseSelections(s, t, noSel, sample, sampleCfg)

        ###### Plots for ==1 lepton (trigger level) ######

        # plots += utils.makeMergedPlots([("1mu", oneMuTriggerSel), ("1ele", oneEleTriggerSel)], "1lep", "nJets", EqBin(10, 0, 10), title="Number of jets", var=op.rng_len(s.cleanedJets))
        # plots += utils.makeMergedPlots([("1mu", oneMuTriggerSel), ("1ele", oneEleTriggerSel)], "1lep", "nVtx", EqBin(100, 0, 100), title="Number of primary vertices", var=t.PV.npvs)

        # for sel,lep,name in [(oneMuTriggerSel, s.muon, "1mu"), (oneEleTriggerSel, s.electron, "1ele")]:
            # plots += cp.makeLeptonPlots(sel, lep, name)
            # plots += cp.makeMETPlots(sel, lep, s.corrMET, name)

            # plots.append(Plot.make1D("{}_MET_dPhi".format(name), op.Phi_mpi_pi(s.corrMET.phi - s.origMET.phi), sel,
                # EqBin(200, -3.1416, 3.1416), title="MET corr phi - MET phi", plotopts=utils.getOpts(name)))
            # plots.append(Plot.make2D("{}_MET_pt_corr_vs_nocorr".format(name), (s.corrMET.pt, s.origMET.pt), sel, (EqBin(20, 0, 500), EqBin(20, 0, 500)), xTitle="corr MET pt", yTitle="orig MET pt", plotopts=utils.getOpts(name)))
            # plots.append(Plot.make2D("{}_MET_phi_corr_vs_nocorr".format(name), (s.corrMET.phi, s.origMET.phi), sel, (EqBin(20, -3.1416, 3.1416), EqBin(20, -3.1416, 3.1416)), xTitle="corr MET phi", yTitle="orig MET phi", plotopts=utils.getOpts(name)))

        ###### Cutflow report

        yields = CutFlowReport("yields")
        plots.append(yields)

        ###### Plots for ==1 lepton, >=4 jets (ttbar level) ######

        fourJetSel = op.rng_len(s.cleanedJets) >= 4
        oneMu4JetSel = oneMuTriggerSel.refine("muon_4jets", cut=fourJetSel)
        oneEle4JetSel = oneEleTriggerSel.refine("ele_4jets", cut=fourJetSel)

        # for sel,lep,name in [(oneMu4JetSel, s.muon, "1mu_4j"), (oneEle4JetSel, s.electron, "1ele_4j")]:
        #     plots += cp.makeLeptonPlots(sel, lep, name)
        #     plots += cp.makeJetPlots(sel, s.cleanedJets, name)
        #     plots += cp.makeMETPlots(sel, lep, s.corrMET, name)
        #     plots += cp.makeBJetPlots(sel, s.cleanedJetsByDeepFlav, name + "_byDeepFlav")

        ##### Plots for ==1 lepton, >=4 jets, >= 2 b jets (ttbar level, with b tagging) ######

        oneMu4Jet2BSel = oneMu4JetSel.refine("muon_4jets_2b", cut=op.rng_len(s.bJetsM) >= 2, weight=s.bTagWeight)
        oneEle4Jet2BSel = oneEle4JetSel.refine("ele_4jets_2b", cut=op.rng_len(s.bJetsM) >= 2, weight=s.bTagWeight)
        yields.add(oneMu4Jet2BSel, "1mu4j2b")
        yields.add(oneEle4Jet2BSel, "1ele4j2b")
        yields.add(oneMu4Jet2BSel, "1lep4j2b")
        yields.add(oneEle4Jet2BSel, "1lep4j2b")

        plots += utils.makeMergedPlots(
            [(f"1mu_4j_2b", oneMu4Jet2BSel), (f"1ele_4j_2b", oneEle4Jet2BSel)],
            f"1lep_4j_2b", "nVtx", EqBin(100, 0, 100), title="Number of primary vertices", var=t.PV.npvs)

        plots += utils.makeMergedPlots(
            [(f"1mu_4j_2b", oneMu4Jet2BSel), (f"1ele_4j_2b", oneEle4Jet2BSel)],
            f"1lep_4j_2b", "nJets", EqBin(6, 4, 10), title="Number of jets", var=op.rng_len(s.cleanedJets))

        for sel,lep,name in [(oneMu4Jet2BSel, s.muon, f"1mu_4j_2b"), (oneEle4Jet2BSel, s.electron, f"1ele_4j_2b")]:
            plots += cp.makeLeptonPlots(sel, lep, name, binScaling=2)
            plots += cp.makeJetPlots(sel, s.cleanedJets, name, binScaling=2, allJets=True)
            # plots += cp.makeBJetPlots(sel, s.cleanedJetsByDeepFlav, name + "_byDeepFlav")
            plots += cp.makeMETPlots(sel, lep, s.corrMET, name, binScaling=2)
            # plots += cp.makeHEMPlots(sel, lep, s.cleanedJets, name)

        ##### Plots for ==1 lepton, >=5 jets, >= 3 b jets (ttb level) ######

        oneMu5Jet3BSel = oneMu4Jet2BSel.refine(f"muon_5jets_3b", cut=op.AND(op.rng_len(s.cleanedJets) >= 5, op.rng_len(s.bJetsM) >= 3))
        oneEle5Jet3BSel = oneEle4Jet2BSel.refine(f"ele_5jets_3b", cut=op.AND(op.rng_len(s.cleanedJets) >= 5, op.rng_len(s.bJetsM) >= 3))

        plots += utils.makeMergedPlots(
            [(f"1mu_5j_3b", oneMu5Jet3BSel), (f"1ele_5j_3b", oneEle5Jet3BSel)],
            f"1lep_5j_3b", "nJets", EqBin(8, 5, 13), title="Number of jets", var=op.rng_len(s.cleanedJets))

        plots += utils.makeMergedPlots(
            [(f"1mu_5j_3b", oneMu5Jet3BSel), (f"1ele_5j_3b", oneEle5Jet3BSel)],
            f"1lep_5j_3b", "nBDeepFlavM", EqBin(3, 2, 5), title="Number of deepFlavourM b jets", var=op.rng_len(s.bJetsM))

        ##### Plots for ==1 lepton, >=6 jets, >= 2 b jets (ttjj level) ######

        oneMu6Jet2BSel = oneMu4Jet2BSel.refine(f"muon_6jets_2b", cut=[ op.rng_len(s.cleanedJets) >= 6 ])
        oneEle6Jet2BSel = oneEle4Jet2BSel.refine(f"ele_6jets_2b", cut=[ op.rng_len(s.cleanedJets) >= 6 ])
        yields.add(oneMu6Jet2BSel, "1mu6j2b")
        yields.add(oneEle6Jet2BSel, "1ele6j2b")
        yields.add(oneMu6Jet2BSel, "1lep6j2b")
        yields.add(oneEle6Jet2BSel, "1lep6j2b")

        plots += utils.makeMergedPlots(
            [(f"1mu_6j_2b", oneMu6Jet2BSel), (f"1ele_6j_2b", oneEle6Jet2BSel)],
            f"1lep_6j_2b", "nBDeepFlavM", EqBin(3, 2, 5), title="Number of deepFlavourM b jets", var=op.rng_len(s.bJetsM))

        for sel,lep,name in [(oneMu6Jet2BSel, s.muon, f"1mu_6j_2b"), (oneEle6Jet2BSel, s.electron, f"1ele_6j_2b")]:
            plots += cp.makeLeptonPlots(sel, lep, name, binScaling=2)
            plots += cp.makeJetPlots(sel, s.cleanedJets, name, maxJet=6, binScaling=2, allJets=True)
            plots += cp.makeBJetPlots(sel, s.cleanedJetsByDeepFlav, name + "_byDeepFlav")
            # plots += cp.makeMETPlots(sel, lep, s.corrMET, name, binScaling=2)
            # plots += cp.makeHEMPlots(sel, lep, cleanedJets, name)

        ##### Plots for ==1 lepton, >=6 jets, >= 4 b jets (ttbb level) ######

        oneMu6Jet4BSel = oneMu6Jet2BSel.refine(f"muon_6jets_4b", cut=op.rng_len(s.bJetsM) >= 4)
        oneEle6Jet4BSel = oneEle6Jet2BSel.refine(f"ele_6jets_4b", cut=op.rng_len(s.bJetsM) >= 4)
        yields.add(oneMu6Jet4BSel, "1mu6j4b")
        yields.add(oneEle6Jet4BSel, "1ele6j4b")
        yields.add(oneMu6Jet4BSel, "1lep6j4b")
        yields.add(oneEle6Jet4BSel, "1lep6j4b")

        # Find couple of extra bjets
        bJetPairs = op.combine(s.bJetsM, N=2)
        minDRbbPair = op.rng_min_element_by(bJetPairs, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        maxMbbPair = op.rng_max_element_by(bJetPairs, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))

        plots += utils.makeMergedPlots(
            [(f"1mu_6j_4b", oneMu6Jet4BSel), (f"1ele_6j_4b", oneEle6Jet4BSel)],
            f"1lep_6j_4b", "nJets", EqBin(7, 6, 13), title="Number of jets", var=op.rng_len(s.cleanedJets))

        for sel,lep,name in [(oneMu6Jet4BSel, s.muon, f"1mu_6j_4b"), (oneEle6Jet4BSel, s.electron, f"1ele_6j_4b")]:
            plots += cp.makeLeptonPlots(sel, lep, name, binScaling=2)
            plots += cp.makeJetPlots(sel, s.cleanedJets, name, maxJet=6, binScaling=2, allJets=True)
            # plots += cp.makeMETPlots(sel, lep, s.corrMET, name, binScaling=2)

        plots += utils.makeMergedPlots(
            [("1mu_6j_4b", oneMu6Jet4BSel), ("1ele_6j_4b", oneEle6Jet4BSel)], "1lep_6j_4b", "average_DRbb",
            EqBin(30, 0.6, 4.), op.rng_mean(op.map(bJetPairs, lambda p: op.deltaR(p[0].p4, p[1].p4))), title="Average #Delta R(bb)")
        plots += utils.makeMergedPlots(
            [("1mu_6j_4b", oneMu6Jet4BSel), ("1ele_6j_4b", oneEle6Jet4BSel)], "1lep_6j_4b", "largest_Mbb",
            EqBin(30, 60, 1560), op.invariant_mass(maxMbbPair[0].p4, maxMbbPair[1].p4), title="Largest M(bb)")

        plots += cp.makeMergedExtraJetPlots(oneMu6Jet4BSel, "1mu_6j_4b", oneEle6Jet4BSel, "1ele_6j_4b", minDRbbPair, "1lep_6j_4b")

        ##### Plots for ttb, ttbb with extra light jet #####

        oneMu6Jet3B3LSel = oneMu6Jet2BSel.refine(f"muon_6jets_3b_3l", cut=op.AND(op.rng_len(s.bJetsM) >= 3, op.rng_len(s.lightJetsM) >= 3))
        oneEle6Jet3B3LSel = oneEle6Jet2BSel.refine(f"ele_6jets_3b_3l", cut=op.AND(op.rng_len(s.bJetsM) >= 3, op.rng_len(s.lightJetsM) >= 3))
        oneMu7Jet4B3LSel = oneMu6Jet4BSel.refine(f"muon_7jets_4b_3l", cut=op.AND(op.rng_len(s.cleanedJets) >= 7, op.rng_len(s.lightJetsM) >= 3))
        oneEle7Jet4B3LSel = oneEle6Jet4BSel.refine(f"ele_7jets_4b_3l", cut=op.AND(op.rng_len(s.cleanedJets) >= 7, op.rng_len(s.lightJetsM) >= 3))

        # Extra light jets -> reconstruct hadronic W
        matchingChi2 = lambda w: op.pow((op.invariant_mass(w) - 82.8) / 11.7, 2.)
        lightJetPairs = op.combine(s.lightJetsM, N=2)
        wCand = op.rng_min_element_by(lightJetPairs, lambda pair: matchingChi2(pair[0].p4 + pair[1].p4))
        lightJets_notW = op.select(s.lightJetsM, lambda j: op.NOT(op.OR(j == wCand[0], j == wCand[1])))

        for selN,sels in [("5j_3b_3l", (oneMu6Jet3B3LSel, oneEle6Jet3B3LSel)), ("7j_4b_3l", (oneMu7Jet4B3LSel, oneEle7Jet4B3LSel))]:
            plots += utils.makeMergedPlots(
                [(f"1mu_{selN}", sels[0]), (f"1ele_{selN}", sels[1])], f"1lep_{selN}", "wMatch_chi2",
                EqBin(30, 0, 10), matchingChi2(wCand[0].p4 + wCand[1].p4), title="W matching chi2")
            plots += utils.makeMergedPlots(
                [(f"1mu_{selN}", sels[0]), (f"1ele_{selN}", sels[1])], f"1lep_{selN}", "wCand_Mjj",
                EqBin(30, 20, 260), op.invariant_mass(wCand[0].p4, wCand[1].p4), title="W candidate mass (GeV)")
            plots += utils.makeMergedPlots(
                [(f"1mu_{selN}", sels[0]), (f"1ele_{selN}", sels[1])], f"1lep_{selN}", "extraLightJet_pt",
                EqBin(30, 30, 730), lightJets_notW[0].pt, title="Add. light jet p_{T} (GeV)")
            plots += utils.makeMergedPlots(
                [(f"1mu_{selN}", sels[0]), (f"1ele_{selN}", sels[1])], f"1lep_{selN}", "extraLightJet_eta",
                EqBin(30, -2.4, 2.4), lightJets_notW[0].eta, title="Add. light jet eta")

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super(controlPlotter, self).postProcess(taskList, config, workdir, resultsdir,
             makeBU=True,
             removeBatch=True,
             createEnvelope=True,
             moveSystHists=True,
             removeFileList=True,
             remove4F5Foverlap=True)
        self.runPlotIt(taskList, config, workdir, resultsdir)
