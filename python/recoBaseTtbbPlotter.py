import logging
logger = logging.getLogger("ttbb plotter")

from bamboo import treefunctions as op
from bamboo.root import gbl as ROOT
from bamboo.root import loadHeader

import definitions as defs
import utils
from baseTtbbPlotter import baseTtbbPlotter

class recoBaseTtbbPlotter(baseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)
        loadHeader("BTagEffEvaluator.h")

    def addArgs(self, parser):
        super().addArgs(parser)
        parser.add_argument("--sf-patches", choices=['none', 'tth', '3d'], required=True, help="Apply b-tag SF patches")

    def prepareTree(self, tree, sample=None, sampleCfg=None,
            nJetNBHTRwgtDir="/t3home/swertz/swertz/bambooOutput/200702_bTagPatch3D/results/", **kwargs):
        tree,noSel,be,lumiArgs = super().prepareTree(tree, sample=sample, sampleCfg=sampleCfg, **kwargs)
        era = sampleCfg["era"]

        # Rochester correction on-the-fly calculation
        defs.addMuonRocCor(be, tree._Muon, era, sample, self.isMC(sample))

        if self.isMC(sample):
            # self.bTagEff = op.define("BTagEffEvaluator", 'const auto <<name>> = BTagEffEvaluator("/t3home/swertz/swertz/bambooOutput/200113_bTagEffs/results/efficiencies.root", {0.277});')

            if self.args.sf_patches == "3d":
                # 3D b-tagging patches
                self.nJetNBHTRwgt = op.define("HistogramEvaluator<double,double,double>", f'const auto <<name>> = HistogramEvaluator<double,double,double>("/t3home/swertz/swertz/bambooOutput/201009_bTag_patch_eff/results/summedProcesses_{era}_ratios.root", "1lep_nJets_nBFlav_HT_sfCorr");')
            elif self.args.sf_patches == "tth":
                # alternatively, 2D ttH b-tagging patches
                self.nJetHTttHRwgt = defs.getNJetHTttHPatches(sample, sampleCfg, era)

        return tree,noSel,be,lumiArgs

    def defineBaseSelections(self, tree, noSel, sample, sampleCfg):
        era = sampleCfg["era"]

        if self.isMC(sample):
            noSel = noSel.refine("puWeight", weight=defs.makePUWeight(tree, era))
        
        noSel = noSel.refine("flags", cut=defs.flagDef(tree.Flag, era, self.isMC(sample)))
        
        _,oneMuTriggerSel = defs.buildMuonSelections(tree, noSel, self.muons, self.vetoMuons, self.electrons, self.vetoElectrons, sample, era, self.isMC(sample))
        _,oneEleTriggerSel = defs.buildElectronSelections(tree, noSel, self.muons, self.vetoMuons, self.electrons, self.vetoElectrons, sample, era, self.isMC(sample))

        return oneMuTriggerSel,oneEleTriggerSel

    def defineObjects(self, tree, noSel, sample=None, sampleCfg=None, **kwargs):
        super().defineObjects(tree, noSel, sample, sampleCfg, **kwargs)
        era = sampleCfg["era"]

        self.origMET = tree.METFixEE2017 if era == "2017" else tree.MET
        self.corrMET = defs.corrMET(self.origMET, tree.PV, sample, era, self.isMC(sample))

        ##### Lepton definition and scale factors
        self.muons = op.select(tree.Muon, defs.muonDef(era))
        self.muon = self.muons[0]
        self.electrons = op.select(tree.Electron, defs.eleDef(era))
        self.electron = self.electrons[0]

        self.vetoMuons = op.select(tree.Muon, defs.vetoMuonDef)
        self.vetoElectrons = op.select(tree.Electron, defs.vetoEleDef)

        ##### Jet definition
        self.rawJets = op.select(tree.Jet, defs.jetDef)
        self.cleanedJets = defs.cleanJets(self.rawJets, self.muons, self.electrons)
        self.HT = op.rng_sum(self.cleanedJets, lambda j: j.pt)

        ##### B tagging definition

        # order jets by *decreasing* deepFlavour
        self.cleanedJetsByDeepFlav = op.sort(self.cleanedJets, lambda jet: -jet.btagDeepFlavB)
        # DeepFlavour Medium tagged jets
        self.bJetsM = defs.bTagDef(self.cleanedJets, era, "M", "btagDeepFlavB")
        self.lightJetsM = defs.lightTagDef(self.cleanedJets, era, "M", "btagDeepFlavB")

        if self.isMC(sample):
            self.bFlavJets = op.select(self.cleanedJets, lambda j: j.hadronFlavour == 5)
            self.cFlavJets = op.select(self.cleanedJets, lambda j: j.hadronFlavour == 4)
            self.lFlavJets = op.select(self.cleanedJets, lambda j: j.hadronFlavour == 0)
            self.quarkJets = op.select(self.cleanedJets, lambda j: op.AND(j.hadronFlavour == 0, op.abs(j.partonFlavour) < 4))
            self.gluonJets = op.select(self.cleanedJets, lambda j: op.AND(j.hadronFlavour == 0, j.partonFlavour == 21))

        # B-tag reweighting: compute product of the shape SFs for all selected jets
        # Also define weight from scale factors for fixed working point for comparison
        if self.isMC(sample):
            # iterativeFit SFs
            self.bTagWeight = [ op.rng_product(self.cleanedJets, lambda jet: jet.btagSF_deepjet_shape) ]

            if self.args.sf_patches == "3d":
                # Apply 3D b-tag SF patches
                self.bTagWeight.append(
                    self.nJetNBHTRwgt.evaluate(
                        op.min(op.rng_len(s.cleanedJets), op.static_cast("unsigned long", op.c_int(8))),
                        op.min(op.rng_len(s.bFlavJets), op.static_cast("unsigned long", op.c_int(4))),
                        op.min(s.HT, op.static_cast("float", op.c_float(1000.)))
                    )
                )
            elif self.args.sf_patches == "tth" and self.nJetHTttHRwgt:
                # Alternatively, apply 2D ttH patches
                self.bTagWeight.append(
                    self.nJetHTttHRwgt.evaluate(
                        op.min(s.HT, op.static_cast("float", op.c_float(1000.))),
                        op.min(op.rng_len(s.cleanedJets), op.static_cast("unsigned long", op.c_int(8)))
                    )
                )

            # fixed WP SFs (TODO)
            # self.bTagSFPerJet = op.map(cleanedJets, lambda j: self.bTagEff.evaluate(j.hadronFlavour, j.btagDeepFlavB, j.pt, op.abs(j.eta), j.btagSF_deepjet_M))
            # self.bTagWeightFixWP = op.rng_product(bTagSFPerJet)
        else:
            self.bTagWeight = []
            self.bTagWeightFixWP = []


    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None, **kwargs):
        super().postProcess(taskList, config, workdir, resultsdir, **kwargs)
