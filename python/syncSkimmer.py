import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.analysismodules import NanoAODSkimmerModule

from bamboo import treefunctions as op

import definitions as defs

# FIXME
# For 2017 we need to loop over the electron collection since we can't be sure
# we have at least one.
def eleTriggerDef(TrigObj, HLT, electrons, sample, era, isMC):
    cuts = []
    if era == '2016':
        cuts.append(HLT.Ele27_WPTight_Gsf)
    if era == '2017':
        cuts.append(op.OR(
            op.AND(HLT.Ele32_WPTight_Gsf_L1DoubleEG, op.rng_any(electrons, lambda ele: op.rng_any(TrigObj, lambda obj: op.AND(op.deltaR(obj.p4, ele.p4) < 0.1, obj.filterBits & 1024)))),
            HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    if era == '2018':
        cuts.append(op.OR(HLT.Ele32_WPTight_Gsf, HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    if not isMC:
        if era == '2016' or era == '2017':
            cuts.append(op.c_bool("SingleElectron" in sample))
        if era == '2018':
            cuts.append(op.c_bool("EGamma" in sample)) # only called EGamma in 2018
    return cuts

class syncSkimmer(NanoAODSkimmerModule):
    def __init__(self, args):
        super(syncSkimmer, self).__init__(args)

    def prepareTree(self, tree, sample=None, sampleCfg=None):
        return NanoAODSkimmerModule.prepareTree(self, tree, sample=sample, sampleCfg=sampleCfg, description=defs.getNanoAODDescription(sampleCfg["era"], self.isMC(sample)), lazyBackend=True)

    def defineSkimSelection(self, t, noSel, sample=None, sampleCfg=None):
        era = sampleCfg["era"]

        # variables to keep from the input tree
        varsToKeep = {"run": None, "luminosityBlock": None, "event": None}

        if not self.isMC(sample):
            if era == "2016":
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 274335, t.luminosityBlock >= 90, t.luminosityBlock <= 94))
            elif era == "2017":
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 297503, t.luminosityBlock >= 5, t.luminosityBlock <= 8))
            elif era == "2018":
                # noSel = noSel.refine("forCsv", cut=op.OR(t.run == 316187, t.run == 316985))
                # noSel = noSel.refine("forCsv", cut=op.AND(t.run == 316187, t.luminosityBlock == 71))
                noSel = noSel.refine("forCsv", cut=op.AND(t.run == 316060, t.luminosityBlock >= 1, t.luminosityBlock <= 6))
        elif sample.startswith("TTToSemiLeptonic") and self.isMC(sample):
            if era == "2016":
                noSel = noSel.refine("forCsv", cut=op.AND(t.luminosityBlock < 80))
            elif era == "2017":
                noSel = noSel.refine("forCsv", cut=op.AND(t.luminosityBlock < 20))
            elif era == "2018":
                noSel = noSel.refine("forCsv", cut=op.AND(t.luminosityBlock < 5))
        else:
            logger.warning("Could not restrict event range!")

        ##### Lepton definition

        muons = op.select(t.Muon, defs.muonDef(era))
        muon = muons[0]
        electrons = op.select(t.Electron, defs.eleDef(era))
        electron = electrons[0]

        vetoMuons = op.select(t.Muon, defs.vetoMuonDef)
        vetoElectrons = op.select(t.Electron, defs.vetoEleDef)
        # vetoElectrons = op.sort(op.select(t.Electron, defs.vetoEleDef), lambda ele: -ele.pt)
        # muon = vetoMuons[0]
        # electron = vetoElectrons[0]

        oneMuSel, oneMuTriggerSel = defs.buildMuonSelections(t, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, self.isMC(sample))

        oneEleSel, oneEleTriggerSel = defs.buildElectronSelections(t, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, self.isMC(sample))

        oneLepSel = noSel.refine("oneLep", cut=op.OR(oneMuSel.cut, oneEleSel.cut))
        # oneLepSel = noSel.refine("oneLep", cut=op.rng_len(vetoElectrons) >= 1)

        ##### Jet definition

        jets = op.select(t.Jet, defs.jetDef)
        cleanedJets = op.sort(defs.cleanJets(jets, muons, electrons), lambda j: -j.pt)
        bJets = defs.bTagDef(cleanedJets, era)

        ##### define our variables

        varsToKeep["is_e"] = op.static_cast("UInt_t", oneEleSel.cut)
        varsToKeep["is_mu"] = op.static_cast("UInt_t", oneMuSel.cut)

        varsToKeep["flags"] = op.static_cast("UInt_t", op.AND(*defs.flagDef(t.Flag, era, self.isMC(sample))))
        varsToKeep["trigger_e"] = op.static_cast("UInt_t", op.switch(oneEleSel.cut, op.AND(*eleTriggerDef(t.TrigObj, t.HLT, electrons, sample, era, self.isMC(sample))), op.c_bool(False)))
        varsToKeep["trigger_mu"] = op.static_cast("UInt_t", op.switch(oneMuSel.cut, op.AND(*defs.muonTriggerDef(t.HLT, sample, era, self.isMC(sample))), op.c_bool(False)))

        def getLepV(attr):
            # return op.switch(oneLepSel.cut, getattr(electron, attr), getattr(muon, attr))
            return op.switch(oneEleSel.cut, getattr(electron, attr), getattr(muon, attr))

        # varsToKeep["lepton_pt_uncorr"] = op.switch(oneLepSel.cut, electron.pt/electron.eCorr, muon.pt)
        varsToKeep["lepton_pt_uncorr"] = op.switch(oneEleSel.cut, electron.pt/electron.eCorr, muon.pt)
        varsToKeep["lepton_pt"] = getLepV("pt")
        varsToKeep["lepton_eta"] = getLepV("eta")
        # varsToKeep["lepton_deltaEtaSC"] = op.switch(oneLepSel.cut, electron.deltaEtaSC, op.c_float(1.))
        varsToKeep["lepton_deltaEtaSC"] = op.switch(oneEleSel.cut, electron.deltaEtaSC, op.c_float(-1))
        varsToKeep["lepton_phi"] = getLepV("phi")
        varsToKeep["lepton_abs_dxy"] = op.abs(getLepV("dxy"))
        varsToKeep["lepton_abs_dz"] = op.abs(getLepV("dz"))
        # varsToKeep["lepton_iso"] = op.switch(oneLepSel.cut, electron.pfRelIso03_all, muon.pfRelIso04_all)
        varsToKeep["lepton_iso"] = op.switch(oneEleSel.cut, electron.pfRelIso03_all, muon.pfRelIso04_all)
        # varsToKeep["lepton_id_tight"] = op.static_cast("UInt_t", electron.cutBased == 4)

        def getJetV(n, attr, jetColl=cleanedJets):
            return op.switch(op.rng_len(jetColl) >= n, getattr(jetColl[n-1], attr), op.c_float(-1))

        varsToKeep["nJets"] = op.static_cast("UInt_t", op.rng_len(cleanedJets))
        varsToKeep["jet1_pt"] = getJetV(1, "pt")
        varsToKeep["jet1_eta"] = getJetV(1, "eta")
        varsToKeep["jet1_phi"] = getJetV(1, "phi")
        varsToKeep["jet1_deepJet"] = getJetV(1, "btagDeepFlavB")
        varsToKeep["jet2_pt"] = getJetV(2, "pt")
        varsToKeep["jet2_eta"] = getJetV(2, "eta")
        varsToKeep["jet2_phi"] = getJetV(2, "phi")
        varsToKeep["jet2_deepJet"] = getJetV(2, "btagDeepFlavB")
        varsToKeep["nBJets_deepJetM"] = op.static_cast("UInt_t", op.rng_len(bJets))

        varsToKeep["nPV"] = t.PV.npvs

        varsToKeep["MET_t1_pt"] = t._METFixEE2017["nom"].pt if era == "2017" else t._MET["nom"].pt
        varsToKeep["MET_t1_phi"] = t._METFixEE2017["nom"].phi if era == "2017" else t._MET["nom"].phi
        corrMET = defs.corrMET(t._METFixEE2017["nom"] if era == "2017" else t._MET["nom"], t.PV, sample, era, self.isMC(sample))
        varsToKeep["MET_xy_pt"] = corrMET.pt
        varsToKeep["MET_xy_phi"] = corrMET.phi

        if self.isMC(sample):
            varsToKeep["jet1_deepJet_shapeSF"] = getJetV(1, "btagSF_deepjet_shape")
            varsToKeep["jet2_deepJet_shapeSF"] = getJetV(2, "btagSF_deepjet_shape")
            varsToKeep["deepJet_shapeSF_weight"] = op.rng_product(cleanedJets, lambda jet: jet.btagSF_deepjet_shape)

            eleRecoSF = defs.getScaleFactor("lepton", (era, "electron_reco"), isElectron=True, systName="ele_reco", defineOnFirstUse=False)
            eleIDSF = defs.getScaleFactor("lepton", (era, "electron_ID", "cut_tight"), isElectron=True, systName="ele_ID", defineOnFirstUse=False)
            muonIDSF = defs.getScaleFactor("lepton", (era, "muon_ID"), combine="weight", systName="muon_ID", defineOnFirstUse=False)
            muonIsoSF = defs.getScaleFactor("lepton", (era, "muon_iso"), combine="weight", systName="muon_iso", defineOnFirstUse=False)
            varsToKeep["lepton_SF"] = op.switch(oneEleSel.cut, eleRecoSF(electron) * eleIDSF(electron), muonIDSF(muon) * muonIsoSF(muon))

            eleTriggerSF = defs.getScaleFactor("lepton", (era, "electron_trigger"), isElectron=True, systName="ele_trigger", defineOnFirstUse=False)
            muonTriggerSF = defs.getScaleFactor("lepton", (era, "muon_trigger"), combine="weight", systName="muon_trigger", defineOnFirstUse=False)
            varsToKeep["trigger_SF"] = op.switch(oneEleSel.cut, eleTriggerSF(electron) * (0.991 if era == "2017" else 1.), muonTriggerSF(muon))
            
            varsToKeep["L1_prefire_weight"] = op.c_float(1.) if era == "2018" else defs.getL1PrefiringSystematic(t)

            varsToKeep["pu_nTrueInt"] = t.Pileup_nTrueInt
            varsToKeep["pu_weight"] = defs.makePUWeight(t, era)

            varsToKeep["MET_smeared_pt"] = t._METFixEE2017["jer"].pt if era == "2017" else t._MET["jer"].pt
            varsToKeep["MET_smeared_phi"] = t._METFixEE2017["jer"].phi if era == "2017" else t._MET["jer"].phi
        else:
            for var in ["jet1_deepJet_shapeSF", "jet2_deepJet_shapeSF", "deepJet_shapeSF_weight", "lepton_SF", "trigger_SF", "L1_prefire_weight", "pu_nTrueInt", "pu_weight", "MET_smeared_pt", "MET_smeared_phi"]:
                varsToKeep[var] = op.c_float(-1)

        return oneLepSel, varsToKeep
