from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo import treefunctions as op

import definitions as defs
import utils

def makeLeptonPlots(sel, lepton, uname, binScaling=1):
    plots = []

    if "mu" in uname:
        flav = "Muon"
    if "ele" in uname:
        flav = "Electron"

    plots.append(Plot.make1D(f"{uname}_lep_pt", lepton.pt, sel,
            EqBin(60 // binScaling, 30., 530.), title="%s p_{T} (GeV)" % flav,
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{uname}_lep_eta", lepton.eta, sel,
            EqBin(50 // binScaling, -2.4, 2.4), title="%s eta" % flav,
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    plots.append(Plot.make1D(f"{uname}_lep_phi", lepton.phi, sel,
            EqBin(50 // binScaling, -3.1416, 3.1416), title="%s phi" % flav,
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    # if flav == "Electron":
    #     plots.append(Plot.make1D(f"{uname}_lep_pt_uncorr", lepton.pt / lepton.eCorr, sel,
    #         EqBin(60 // binScaling, 30., 530.), title="%s p_{T} (GeV)" % flav, plotopts=utils.getOpts(uname)))
    #     plots.append(Plot.make1D(f"{uname}_lep_iso", lepton.pfRelIso03_all, sel,
    #         EqBin(50 // binScaling, 0, 0.1), title="%s isolation" % flav, plotopts=utils.getOpts(uname)))
    #     plots.append(Plot.make1D(f"{uname}_lep_dxy", lepton.dxy, sel,
    #         EqBin(100 // binScaling, -0.1, 0.1), title="%s dxy" % flav, plotopts=utils.getOpts(uname)))
    #     plots.append(Plot.make1D(f"{uname}_lep_dz", lepton.dz, sel,
    #         EqBin(100 // binScaling, -0.2, 0.2), title="%s dz" % flav, plotopts=utils.getOpts(uname)))
    # if flav == "Muon":
    #     plots.append(Plot.make1D(f"{uname}_lep_iso", lepton.pfRelIso04_all, sel,
    #         EqBin(50 // binScaling, 0, 0.15), title="%s isolation" % flav, plotopts=utils.getOpts(uname)))

    return plots

def makeJetPlots(sel, jets, uname, maxJet=4, binScaling=1, allJets=False):
    plots = []

    if allJets:
        plots.append(Plot.make1D(f"{uname}_jets_pt", op.map(jets, lambda j: j.pt), sel,
                EqBin(60 // binScaling, 30., 730.), title="Jets p_{T} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{uname}_jets_eta", op.map(jets, lambda j: j.eta), sel,
                EqBin(50 // binScaling, -2.4, 2.4), title=f"Jets #eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
        # plots.append(Plot.make1D(f"{uname}_jets_phi", op.map(jets, lambda j: j.phi), sel,
        #         EqBin(50 // binScaling, -3.1416, 3.1416), title="Jets #phi", plotopts=utils.getOpts(uname, **{"log-y": False})))

    for i in range(maxJet):
        plots.append(Plot.make1D(f"{uname}_jet{i+1}_pt", jets[i].pt, sel,
                EqBin(60 // binScaling, 30., 730. - min(4, i) * 100), title=f"{utils.getCounter(i+1)} jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{uname}_jet{i+1}_eta", jets[i].eta, sel,
                EqBin(50 // binScaling, -2.4, 2.4), title=f"{utils.getCounter(i+1)} jet eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
        # plots.append(Plot.make1D(f"{uname}_jet{i+1}_phi", jets[i].phi, sel,
        #         EqBin(50 // binScaling, -3.1416, 3.1416), title=f"{utils.getCounter(i+1)} jet phi", plotopts=utils.getOpts(uname, **{"log-y": False})))
    return plots

def makeExtraJetPlots(sel, jets, uname, binScaling=2, pTthresh=30):
    plots = []

    for i in range(2):
        plots.append(Plot.make1D(f"{uname}_extra_jet{i+1}_pt", jets[i].pt, sel,
                EqBin(60 // binScaling, pTthresh, 600. + pTthresh - i*200), title=f"{utils.getCounter(i+1)} extra jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{uname}_extra_jet{i+1}_eta", jets[i].eta, sel,
                EqBin(50 // binScaling, -2.4, 2.4), title=f"{utils.getCounter(i+1)} extra jet #eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
    plots.append(Plot.make1D(f"{uname}_extra_jet_DR", op.deltaR(jets[0].p4, jets[1].p4),
            sel, EqBin(60 // binScaling, 0.4, 3.), title="Extra jets #Delta R(bb)",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    plots.append(Plot.make1D(f"{uname}_extra_jet_M", op.invariant_mass(jets[0].p4, jets[1].p4),
            sel, EqBin(60 // binScaling, 15., 435.), title="Extra jets M(bb) (GeV)",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{uname}_extra_jet_pT", (jets[0].p4 + jets[1].p4).Pt(),
            sel, EqBin(60 // binScaling, 20., 620.), title="Extra jets p_{T}(bb) (GeV)",
            plotopts=utils.getOpts(uname)))

    return plots

def makeMergedExtraJetPlots(sel1, selName1, sel2, selName2, jets, uname):
    plots = []

    for i in range(2):
        plots += utils.makeMergedPlots([(selName1, sel1), (selName2, sel2)],
            uname, f"extra_jet{i+1}_pt", EqBin(30, 30., 630. - i*200),
            var=jets[i].pt, title=f"{utils.getCounter(i+1)} extra jet p_{{T}} (GeV)")
        plots += utils.makeMergedPlots([(selName1, sel1), (selName2, sel2)],
            uname, f"extra_jet{i+1}_eta", EqBin(30, -2.4, 2.4),
            var=jets[i].eta, title=f"{utils.getCounter(i+1)} extra jet #eta",
            plotopts={"log-y": False})

    plots += utils.makeMergedPlots([(selName1, sel1), (selName2, sel2)],
            uname, "extra_jet_DR", EqBin(30, 0.4, 3.),
            var=op.deltaR(jets[0].p4, jets[1].p4), title="Extra jets #Delta R(bb)",
            plotopts={"log-y": False})
    plots += utils.makeMergedPlots([(selName1, sel1), (selName2, sel2)],
            uname, "extra_jet_M", EqBin(30, 15., 435.),
            var=op.invariant_mass(jets[0].p4, jets[1].p4), title="Extra jets M(bb) (GeV)")
    plots += utils.makeMergedPlots([(selName1, sel1), (selName2, sel2)],
            uname, "extra_jet_pT", EqBin(30, 20., 620.),
            var=(jets[0].p4 + jets[1].p4).Pt(), title="Extra jets p_{T}(bb) (GeV)")

    return plots

def makeBJetPlots(sel, jets, uname):
    plots = []

    for i in range(4):
        plots.append(Plot.make1D(f"{uname}_jet{i+1}_deepFlav", jets[i].btagDeepFlavB,
                sel, EqBin(60, 0., 1.), title=f"{utils.getCounter(i+1)}-highest jet deepFlavour", plotopts=utils.getOpts(uname)))

    return plots

def makeMETPlots(sel, lepton, met, uname, binScaling=1):
    plots = []

    plots.append(Plot.make1D(f"{uname}_MET_pt", met.pt, sel,
            EqBin(60 // binScaling, 0., 600.), title="MET p_{T} (GeV)",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{uname}_MET_phi", met.phi, sel,
            EqBin(60 // binScaling, -3.1416, 3.1416), title="MET #phi",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    # plots.append(Plot.make1D(f"{uname}_MET_lep_deltaPhi",
    #         op.Phi_mpi_pi(lepton.phi - met.phi), sel, EqBin(60 // binScaling, -3.1416, 3.1416),
    #         title="#Delta #phi (lepton, MET)", plotopts=utils.getOpts(uname, **{"log-y": False})))

    MT = op.sqrt( 2. * met.pt * lepton.p4.Pt() * (1. - op.cos(op.Phi_mpi_pi(met.phi - lepton.p4.Phi()))) )
    plots.append(Plot.make1D(f"{uname}_MET_MT", MT, sel,
            EqBin(60 // binScaling, 0., 600.), title="Lepton M_{T} (GeV)",
            plotopts=utils.getOpts(uname)))

    return plots


def makeHEMPlots(sel, lepton, jets, uname):
    plots = []

    jets_eta = op.map(jets, lambda jet: jet.eta)
    jets_phi = op.map(jets, lambda jet: jet.phi)
    jets_pt = op.map(jets, lambda jet: jet.pt)

    jetsHEM = op.select(jets, lambda jet: op.AND(jet.eta < -1.3, jet.phi > -1.57, jet.phi < -0.87))
    jetsHEM_pt = op.map(jetsHEM, lambda jet: jet.pt)
    jetsHEM_sel = sel.refine(uname + "HEM", cut=op.rng_len(jetsHEM) > 0)

    jetsNoHEM = op.select(jets, lambda jet: op.OR(op.AND(jet.eta < -1.3, op.OR(jet.phi < -1.57, jet.phi > -0.87)), jet.eta > 1.3))
    jetsNoHEM_pt = op.map(jetsNoHEM, lambda jet: jet.pt)

    jetsNoHEM_sel = sel.refine(uname + "NoHEM", cut=op.rng_len(jetsNoHEM) > 0)

    plots.append(Plot.make1D(f"{uname}_jet_pt", jets_pt, sel,
            EqBin(30, 30., 730.), title="Jet p_{T} (GeV)", plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{uname}_HEM_jet_pt", jetsHEM_pt, jetsHEM_sel,
            EqBin(30, 30., 730.), title="Jet p_{T} inside HEM failure (GeV)",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{uname}_NoHEM_jet_pt", jetsNoHEM_pt, jetsNoHEM_sel,
            EqBin(30, 30., 730.), title="Jet p_{T} outside HEM failure (GeV)",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make2D(f"{uname}_jet_eta_vs_phi", (jets_eta, jets_phi), sel,
            (EqBin(60, -2.4, 2.4), EqBin(60, -3.1416, 3.1416)), xTitle="Jet eta", yTitle="Jet phi", plotopts=utils.getOpts(uname)))
    plots.append(Plot.make2D(f"{uname}_lep_eta_vs_phi", (lepton.eta, lepton.phi), sel,
            (EqBin(60, -2.4, 2.4), EqBin(60, -3.1416, 3.1416)), xTitle="Lepton eta", yTitle="Lepton phi", plotopts=utils.getOpts(uname)))

    return plots
