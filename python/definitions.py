import os
import re
import math
import HistogramTools as HT
from itertools import chain

from bamboo.plots import Plot, SummedPlot
from bamboo import treefunctions as op
from bamboo import treedecorators as td
from bamboo import scalefactors
from bamboo.analysisutils import makePileupWeight
from bamboo.analysisutils import configureRochesterCorrection

import utils

#### Configure NanoAOD decoration

def getNanoAODDescription(era, isMC, doRocCor=True):
    groups = ["PV_", "Flag_", "HLT_"]
    collections = ["nElectron", "nJet", "nMuon", "nTrigObj"]
    if era == "2017":
        groups.append("METFixEE2017_")
        met = "METFixEE2017"
    else:
        groups.append("MET_")
        met = "MET"
    varReaders = []

    if doRocCor:
        varReaders.append(td.nanoRochesterCalc)

    if isMC:
        mcGroups = ["GenMET_", "Generator_"]
        mcCollections = ["nGenDressedLepton", "nGenJet", "nGenPart"]

        exclJetSyst = list(chain.from_iterable(
            [ (f"{j}up", f"{j}down") for j in [
                "jesHF", "jesHF_2018", "jesHF_2017", "jesHF_2016",
                "jesEC2", "jesEC2_2018", "jesEC2_2017", "jesEC2_2016",
                "jer2", "jer3", "jer4", "jer5",]
            ]
        )) + ["raw"]
        jetmet = td.ReadJetMETVar("Jet", met, jetsNomName="nom", metNomName="jer", jetsExclVars=exclJetSyst, metExclVars=['nom', 'raw'], bTaggers=["deepjet"], bTagWPs=["L", "M", "T", "shape"])
        varReaders.append(jetmet)
        return td.NanoAODDescription(groups=groups + mcGroups, collections=collections + mcCollections, systVariations=varReaders)
    else:
        varReaders.append(td.ReadJetMETVar("Jet", met, jetsNomName="nom", metNomName="nom"))
        return td.NanoAODDescription(groups=groups, collections=collections, systVariations=varReaders)

#### Reco-level object definitions

def flagDef(flags, era, isMC):
    # from https://twiki.cern.ch/twiki/bin/view/CMS/MissingETOptionalFiltersRun2
    cuts = [
        flags.goodVertices,
        flags.globalSuperTightHalo2016Filter,
        flags.HBHENoiseFilter,
        flags.HBHENoiseIsoFilter,
        flags.EcalDeadCellTriggerPrimitiveFilter,
        flags.BadPFMuonFilter
    ]
    if era == '2017' or era == '2018':
        cuts.append(flags.ecalBadCalibFilterV2)
    if not isMC:
        cuts.append(flags.eeBadScFilter)
    return cuts

def addMuonRocCor(be, origMuons, era, sample, isMC):
    rochesterFile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "data", "Rochester", f"RoccoR{era}.txt")
    configureRochesterCorrection(origMuons, rochesterFile, isMC=isMC, backend=be, uName=sample)

def muonTriggerDef(HLT, sample, era, isMC):
    cuts = []
    if era == '2016':
        cuts.append(op.OR(HLT.IsoMu24, HLT.IsoTkMu24))
    if era == '2017':
        cuts.append(HLT.IsoMu27)
    if era == '2018':
        cuts.append(HLT.IsoMu24)
    if not isMC:
        cuts.append(op.c_bool("SingleMuon" in sample))
    return cuts

def eleTriggerDef(TrigObj, HLT, ele, sample, era, isMC):
    cuts = []
    if era == '2016':
        cuts.append(HLT.Ele27_WPTight_Gsf)
    if era == '2017':
        cuts.append(op.OR(
            op.AND(HLT.Ele32_WPTight_Gsf_L1DoubleEG, op.rng_any(TrigObj, lambda obj: op.AND(op.deltaR(obj.p4, ele.p4) < 0.1, obj.filterBits & 1024))),
            HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    if era == '2018':
        cuts.append(op.OR(HLT.Ele32_WPTight_Gsf, HLT.Ele28_eta2p1_WPTight_Gsf_HT150))
    if not isMC:
        if era == '2016' or era == '2017':
            cuts.append(op.c_bool("SingleElectron" in sample))
        if era == '2018':
            cuts.append(op.c_bool("EGamma" in sample)) # only called EGamma in 2018
    return cuts

def muonDef(era):
    def muonDefImpl(mu):
        if era == '2016' or era == '2018':
            ptCut = 26.
        elif era == '2017':
            ptCut = 29.
        return op.AND(
            mu.pt > ptCut,
            op.abs(mu.eta) < 2.4,
            # tight ID
            mu.tightId,
            # tight deltabeta ISO R=0.4
            mu.pfRelIso04_all < 0.15,
        )
    return muonDefImpl

def vetoMuonDef(mu):
    return op.AND(
        mu.pt > 15., op.abs(mu.eta) < 2.4,
        # loose ID
        mu.looseId,
        # loose deltabeta ISO R=0.4
        mu.pfRelIso04_all < 0.25,
    )

def eleDef(era):
    def eleDefImpl(ele):
        absEtaSC = op.abs(ele.eta + ele.deltaEtaSC)
        ele_pt = ele.pt # / ele.eCorr # uncomment to use uncalibrated electron pt
        if era == '2016':
            eraCut = op.AND(ele_pt > 29, op.abs(ele.eta) < 2.4)
        if era == '2017' or era == '2018':
            eraCut = op.OR(
                op.AND(ele_pt > 34., op.abs(ele.eta) < 2.4),
                op.AND(ele_pt > 30., op.abs(ele.eta) < 2.1),
            )
        return op.AND(
            eraCut,
            op.OR(absEtaSC < 1.4442, absEtaSC > 1.566),
            # "average" d0 and dz cuts, to be tuned?
            op.OR(
                # barrel
                op.AND(absEtaSC <= 1.479, op.abs(ele.dxy) < 0.05, op.abs(ele.dz) < 0.1),
                # endcap
                op.AND(absEtaSC > 1.479, op.abs(ele.dxy) < 0.1, op.abs(ele.dz) < 0.2),
            ),
            # tight cut-based ID
            ele.cutBased == 4,
        )
    return eleDefImpl

def vetoEleDef(ele):
    ele_pt = ele.pt # / ele.eCorr # uncomment to use uncalibrated electron pt
    return op.AND(
        ele_pt > 15., op.abs(ele.eta) < 2.5,
        # veto cut-based ID
        ele.cutBased >= 1,
    )

def jetDef(jet):
    return op.AND(
        jet.pt > 30., op.abs(jet.eta) < 2.4,
        # tight lepton veto jet ID
        jet.jetId & 4,
    )

# Clean jets from leptons -> since we have veto'd extra loose leptons we
# don't have to use vetoElectrons/Muons at this point
# However we NEED rng_any since we don't know how many electrons/muons we have (could be 0 or 1)
# Also make sure the jets are sorted by Pt (not guaranteed since JER is applied)
def cleanJets(jets, muons, electrons, sort=True):
    jets = op.select(jets, lambda jet: op.AND(
            op.NOT(op.rng_any(electrons, lambda ele: op.deltaR(jet.p4, ele.p4) < 0.4)),
            op.NOT(op.rng_any(muons, lambda mu: op.deltaR(jet.p4, mu.p4) < 0.4))
        ))
    if sort:
        jets = op.sort(jets, lambda j: -j.pt)
    return jets

bTagWorkingPoints = {
    "2016": {
        "btagDeepFlavB": {
            "L": 0.0614,
            "M": 0.3093,
            "T": 0.7221
        },
        "btagDeepB": { # DeepCSV
            "L": 0.2217,
            "M": 0.6321,
            "T": 0.8953
        },
    },
    "2017": {
        "btagDeepFlavB": {
            "L": 0.0521,
            "M": 0.3033,
            "T": 0.7489
        },
        "btagDeepB": {
            "L": 0.1522,
            "M": 0.4941,
            "T": 0.8001
        },
    },
    "2018": {
        "btagDeepFlavB": {
            "L": 0.0494,
            "M": 0.277,
            "T": 0.7264
        },
        "btagDeepB": {
            "L": 0.1241,
            "M": 0.4184,
            "T": 0.7527
        },
    },
}

def bTagDef(jets, era, wp="M", tagger="btagDeepFlavB"):
    return op.select(jets, lambda jet: getattr(jet, tagger) >= bTagWorkingPoints[era][tagger][wp])

def lightTagDef(jets, era, wp="M", tagger="btagDeepFlavB"):
    return op.select(jets, lambda jet: getattr(jet, tagger) < bTagWorkingPoints[era][tagger][wp])

class corrMET(object):
    def __init__(self, rawMET, pv, sample, era, isMC): #From https://lathomas.web.cern.ch/lathomas/METStuff/XYCorrections/XYMETCorrection.h (preliminary)
        if isMC: 
            if era == "2016":
                xcorr = (0.195191, 0.170948)
                ycorr = (0.0311891, -0.787627)
            if era == "2017":
                xcorr = (0.182569, -0.276542)
                ycorr = (-0.155652, 0.417633)
            if era == "2018":
                xcorr = (-0.296713, 0.141506)
                ycorr = (-0.115685, -0.0128193)
        else:
            runEra = utils.getRunEra(sample)
            if era == "2016":
                if runEra == "B":
                    xcorr = (0.0478335, 0.108032)
                    ycorr = (-0.125148, -0.355672)
                if runEra == "C":
                    xcorr = (0.0916985, -0.393247)
                    ycorr = (-0.151445, -0.114491)
                if runEra == "D":
                    xcorr = (0.0581169, -0.567316)
                    ycorr = (-0.147549, -0.403088)
                if runEra == "E":
                    xcorr = (0.065622, -0.536856)
                    ycorr = (-0.188532, -0.495346)
                if runEra == "F":
                    xcorr = (0.0313322, -0.39866)
                    ycorr = (-0.16081, -0.960177)
                if runEra == "G":
                    xcorr = (-0.040803, 0.290384)
                    ycorr = (-0.0961935, -0.666096)
                if runEra == "H":
                    xcorr = (-0.0330868, 0.209534)
                    ycorr = (-0.141513, -0.816732)
            if era == "2017":
                if runEra == "B":
                    xcorr = (0.19563, -1.51859)
                    ycorr = (-0.306987, 1.84713)
                if runEra == "C":
                    xcorr = (0.161661, -0.589933)
                    ycorr = (-0.233569, 0.995546)
                if runEra == "D":
                    xcorr = (0.180911, -1.23553)
                    ycorr = (-0.240155, 1.27449)
                if runEra == "E":
                    xcorr = (0.149494, -0.901305)
                    ycorr = (-0.178212, 0.535537)
                if runEra == "F":
                    xcorr = (0.165154, -1.02018)
                    ycorr = (-0.253794, -0.75776)
            if era == "2018":
                if runEra == "A":
                    xcorr = (-0.362865, 1.94505)
                    ycorr = (-0.0709085, 0.307365)
                if runEra == "B":
                    xcorr = (-0.492083, 2.93552)
                    ycorr = (-0.17874, 0.786844)
                if runEra == "C":
                    xcorr = (-0.52134, 1.44544)
                    ycorr = (-0.118956, 1.96434)
                if runEra == "D":
                    xcorr = (-0.531151, 1.37568)
                    ycorr = (-0.0884639, 1.57089)
        METxcorr = xcorr[0] * pv.npvs + xcorr[1]
        METycorr = ycorr[0] * pv.npvs + ycorr[1]
        corrMETx = rawMET.pt * op.cos(rawMET.phi) + METxcorr
        corrMETy = rawMET.pt * op.sin(rawMET.phi) + METycorr
        self.pt = op.sqrt(corrMETx**2 + corrMETy**2)
        atan = op.atan(corrMETy / corrMETx)
        self.phi = op.multiSwitch(
                (corrMETx > 0, atan),
                (corrMETy > 0, atan + math.pi),
                atan - math.pi
            )

# scale factors

def makePUWeight(tree, era):
    if era == "2018":
        puFile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "ScaleFactors", "pileup", "puweights2018_Autumn18.json")
    elif era == "2017":
        puFile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "ScaleFactors", "pileup", "puweights2017_Fall17.json")
    elif era == "2016":
        puFile = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "ScaleFactors", "pileup", "puweights2016_Moriond17.json")
    return makePileupWeight(puFile, tree.Pileup_nTrueInt, systName="pileup")

def localizeSF(aPath, era):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "ScaleFactors", era, aPath)

# get scale factors 

myScaleFactors = {
    "2016": {
        "electron_ID": {"cut_tight": localizeSF("Electron_EGamma_SF2D_CutBased_Tight.json", "2016")},
        "electron_reco": localizeSF("Electron_EGamma_SF2D_Reco.json", "2016"),
        "electron_trigger": localizeSF("Electron_ele27.json", "2016"),
        "muon_ID": [(["Run2016" + r for r in "BCDEF"], localizeSF("Muon_TightID_RunBtoF.json", "2016")),
                (["Run2016" + r for r in "GH"], localizeSF("Muon_TightID_RunGH.json", "2016"))],
        "muon_iso": [(["Run2016" + r for r in "BCDEF"], localizeSF("Muon_TightIso_RunBtoF.json", "2016")),
                (["Run2016" + r for r in "GH"], localizeSF("Muon_TightIso_RunGH.json", "2016"))],
        "muon_trigger": [(["Run2016" + r for r in "BCDEF"], localizeSF("Muon_IsoMu24_OR_IsoTkMu24_RunBtoF.json", "2016")),
                (["Run2016" + r for r in "GH"], localizeSF("Muon_IsoMu24_OR_IsoTkMu24_RunGH.json", "2016"))],
    },
    "2017": {
        "electron_ID": {"cut_tight": localizeSF("Electron_EGamma_SF2D_CutBased_Tight.json", "2017")},
        "electron_reco": localizeSF("Electron_EGamma_SF2D_Reco.json", "2017"),
        "electron_trigger": localizeSF("Electron_ele28_ht150_OR_ele32.json", "2017"),
        "muon_ID": localizeSF("Muon_TightID_RunBtoF.json", "2017"),
        "muon_iso": localizeSF("Muon_TightIso_RunBtoF.json", "2017"),
        "muon_trigger": localizeSF("Muon_IsoMu27_RunBtoF.json", "2017"),
    },
    "2018": {
        "electron_ID": {"cut_tight": localizeSF("Electron_EGamma_SF2D_CutBased_Tight.json", "2018")},
        "electron_reco": localizeSF("Electron_EGamma_SF2D_Reco.json", "2018"),
        "electron_trigger": localizeSF("Electron_ele28_ht150_OR_ele32_etaCut.json", "2018"),
        # "electron_trigger": localizeSF("Electron_ele28_ht150_OR_ele32.json", "2018"),
        "muon_ID": localizeSF("Muon_TightID.json", "2018"),
        "muon_iso": localizeSF("Muon_TightIso.json", "2018"),
        "muon_trigger": [(["Run315264to316360"], localizeSF("Muon_IsoMu24_BeforeMuonHLTUpdate.json", "2018")),
                (["Run316361to325175"], localizeSF("Muon_IsoMu24_AfterMuonHLTUpdate.json", "2018"))],
    },
}

# fill in some defaults: myScalefactors and bamboo.scalefactors.binningVariables_nano
def getScaleFactor(objType, key, periods=None, combine=None, isElectron=False, systName=None, defineOnFirstUse=True):
    return scalefactors.get_scalefactor(objType, key, combine=combine,
        sfLib=myScaleFactors, paramDefs=scalefactors.binningVariables_nano,
        isElectron=isElectron, systName=systName, defineOnFirstUse=defineOnFirstUse)

def getL1PrefiringSystematic(tree):
    return op.systematic(tree.L1PreFiringWeight_Nom, name="L1prefire", up=tree.L1PreFiringWeight_Up, down=tree.L1PreFiringWeight_Dn)

# Return exclusive muon selection (without and with applied trigger), with trigger, ID, and iso scale factors (for MC)
def buildMuonSelections(tree, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, isMC):
    scaleFactors = []
    if isMC:
        muonIDSF = getScaleFactor("lepton", (era, "muon_ID"), combine="weight", systName="muon_ID")
        muonIsoSF = getScaleFactor("lepton", (era, "muon_iso"), combine="weight", systName="muon_iso")
        muonTriggerSF = getScaleFactor("lepton", (era, "muon_trigger"), combine="weight", systName="muon_trigger")
        scaleFactors = [ muonIDSF(muons[0]), muonIsoSF(muons[0]) ]

    oneMuSel = noSel.refine("muon",
                    cut=op.AND(
                        op.rng_len(muons) == 1,
                        op.rng_len(vetoMuons) == 1,
                        op.rng_len(vetoElectrons) == 0
                    ),
                    weight=scaleFactors
                )
    triggerSFWeights = []
    if isMC:
        triggerSFWeights.append(muonTriggerSF(muons[0]))
        if era in ["2016", "2017"]:
            triggerSFWeights.append(getL1PrefiringSystematic(tree))
    oneMuTriggerSel = oneMuSel.refine("muonTrigger",
                                    cut=muonTriggerDef(tree.HLT, sample, era, isMC),
                                    weight=triggerSFWeights)

    return oneMuSel, oneMuTriggerSel

# Return exclusive electron selection (without and with applied trigger), with trigger, ID/iso, and reco scale factors (for MC)
def buildElectronSelections(tree, noSel, muons, vetoMuons, electrons, vetoElectrons, sample, era, isMC):
    scaleFactors = []
    if isMC:
        eleRecoSF = getScaleFactor("lepton", (era, "electron_reco"), isElectron=True, systName="ele_reco")
        eleIDSF = getScaleFactor("lepton", (era, "electron_ID", "cut_tight"), isElectron=True, systName="ele_ID")
        eleTriggerSF = getScaleFactor("lepton", (era, "electron_trigger"), isElectron=True, systName="ele_trigger")
        scaleFactors = [ eleRecoSF(electrons[0]), eleIDSF(electrons[0]) ]

    oneEleSel = noSel.refine("electron",
                    cut=op.AND(
                        op.rng_len(vetoMuons) == 0,
                        op.rng_len(vetoElectrons) == 1,
                        op.rng_len(electrons) == 1
                    ),
                    weight=scaleFactors
                )
    triggerSFWeights = []
    if isMC:
        triggerSFWeights.append(eleTriggerSF(electrons[0]))
        if era in ["2016", "2017"]:
            triggerSFWeights.append(getL1PrefiringSystematic(tree))
        if era == "2017":
            # HLT Z_vtx correction
            triggerSFWeights.append(0.991)
    oneEleTriggerSel = oneEleSel.refine("electronTrigger",
                                    cut=eleTriggerDef(tree.TrigObj, tree.HLT, electrons[0], sample, era, isMC),
                                    weight=triggerSFWeights)

    return oneEleSel, oneEleTriggerSel

def getNJetHTttHPatches(sample, cfg, era):
    if "subprocess" in cfg:
        subProc = cfg["subprocess"]
        if "TTbb_4f" in sample and subProc in ["ttB", "ttbb", "ttbj"]:
            return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttbb__btag_NOMINAL");')
        elif subProc in ["ttB", "ttbb", "ttbj"]:
            return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttbb_5FS__btag_NOMINAL");')
        elif subProc == "ttcc":
            return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttcc__btag_NOMINAL");')
        elif subProc == "ttjj":
            return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttlf__btag_NOMINAL");')
    elif "group" in cfg:
        group = cfg["group"]
        if group == "ttH":
            return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttH__btag_NOMINAL");')
    elif "TTZ" in sample:
        return op.define("HistogramEvaluator<float,float>", f'const auto <<name>> = HistogramEvaluator<float,float>("/t3home/umolinat/btag_patches/btag_sf_patches_sl_{era}_deepJet.root", "SF_Evt_HT_jets_vs_N_Jets__ttZ__btag_NOMINAL");')
    return None
