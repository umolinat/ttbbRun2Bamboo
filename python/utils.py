import os
import re
import json
from copy import deepcopy

import logging
logger = logging.getLogger("utils")

from bamboo.plots import Plot, SummedPlot
from bamboo import treefunctions as op
from bamboo import scalefactors
from bamboo.root import gbl as ROOT
from bamboo.analysismodules import SampleTask

import HistogramTools as HT

PFN_PREFIX = "/pnfs/psi.ch/cms/trivcat"
# PFN_PREFIX = "dcap://t3se01.psi.ch:22125//pnfs/psi.ch/cms/trivcat"
# PFN_PREFIX = "root://t3dcachedb.psi.ch:1094///pnfs/psi.ch/cms/trivcat"

def getOpts(uname, **kwargs):
    label = None
    opts = {}
    if "ele" in uname:
        label = "1 electron"
    elif "mu" in uname:
        label = "1 muon"
    elif "lep" in uname:
        label = "1 lepton (e/#mu)"
    if "gen" in uname:
        label = "Gen: " + label
    if "4j" in uname:
        label += ", #geq 4 jets"
    elif "5j" in uname:
        label += ", #geq 5 jets"
    elif "6j" in uname:
        label += ", #geq 6 jets"
    elif "7j" in uname:
        label += ", #geq 7 jets"
    if "2b" in uname:
        label += ", #geq 2 b"
    elif "3b" in uname:
        label += ", #geq 3 b"
    elif "4b" in uname:
        label += ", #geq 4 b"
    if "3l" in uname:
        label += ", #geq 3 l"
    if label:
        opts = {
            "labels": [{"text": label, "position": [0.205, 0.912]}]
        }
    opts.update(kwargs)
    return opts

def getCounter(i):
    if i <= 0:
        return str(i)
    if i == 1:
        return "1st"
    if i == 2:
        return "2nd"
    if i == 3:
        return "3rd"
    if i >= 4:
        return "{}th".format(i)

def getRunEra(sample):
    """Return run era (A/B/...) for data sample"""
    result = re.search(r'Run201.([A-Z]?)', sample)
    if result is None:
        raise RuntimeError("Could not find run era from sample {}".format(sample))
    return result.group(1)

def makeMergedPlots(categDef, newCat, name, binning, var=None, **kwargs):
    """ Make a series of plots which will be merged.
    - categDef can either be e.g.:
        - `[("mu", muSelection), ("el", elSelection), ...]`, in which case the same variable `var` is used for all sub-categories
        - `[("mu", muSelection, muVar), ("el", elSelection, elVar), ...]`, for cases where the variable is different for each sub-category
    - `newCat`: name of the merged category
    - `name`: name of the merged plot (-> full plot name is newCat_name)
    - `var`: variable to plot (if it is the same for all categories)
    - `binning`: binning to be used
    Any further named args will be forwarded to the plot constructor.
    The variables can also be iterables for multi-dimensional plots.
    """
    plotsToAdd = []
    plotopts = kwargs.pop("plotopts", {})

    for cat in categDef:
        if len(cat) == 2:
            (catName, catSel), catVar = cat, var
        elif len(cat) == 3:
            catName, catSet, catVar = cat
        else:
            raise Exception(f"{cat} should have 2 or 3 entries")
        thisName = f"{catName}_{name}"
        if not hasattr(catVar, "__len__"):
            plotType = Plot.make1D
        elif len(catVar) == 2:
            plotType = Plot.make2D
        elif len(catVar) == 3:
            plotType = Plot.make3D
        newPlotopts = getOpts(catName, **plotopts)
        plotsToAdd.append(plotType(thisName, catVar, catSel, binning, plotopts=newPlotopts, **kwargs))

    newPlotopts = getOpts(newCat, **plotopts)

    return plotsToAdd + [SummedPlot(f"{newCat}_{name}", plotsToAdd, plotopts=newPlotopts, **kwargs)]


#### Common tasks (systematics, sample splittings...)

def addTheorySystematics(plotter, sample, sampleCfg, tree, noSel, MEscale=True, PSISR=True, PSFSR=True, combinedMEscale=False):
    # FIXME
    import yaml
    with open(os.path.join(os.path.dirname(__file__), '..', 'config', 'buggyThSyst.yml')) as file_:
        origName = sample if not "subprocess" in sampleCfg else "".join(sample.split("_" + sampleCfg["subprocess"]))
        buggySamples = yaml.load(file_, Loader=yaml.FullLoader)
        buggySyst = buggySamples.get(origName, "")
        if buggySyst:
            logger.warning(f"Will NOT apply the following uncertainties to sample {sample}: {buggySyst}")

    if MEscale:
        # for muF and muR separately
        if "QCD" not in buggySyst:
            logger.info("Adding separate muF/muR systematics")
            noSel = noSel.refine("qcdMuF", weight=op.systematic(op.c_float(1.), name="qcdMuF", up=tree.LHEScaleWeight[3], down=tree.LHEScaleWeight[5]))
            noSel = noSel.refine("qcdMuR", weight=op.systematic(op.c_float(1.), name="qcdMuR", up=tree.LHEScaleWeight[1], down=tree.LHEScaleWeight[7]))
        plotter.qcdScaleVariations = dict()

    if combinedMEscale:
        # for taking envelope of 7-point variation
        if "QCD" not in buggySyst:
            logger.info("Adding 7-point muF/muR systematics")
            plotter.qcdScaleVariations = { f"qcdScalevar{i}": tree.LHEScaleWeight[i] for i in [0, 1, 3, 5, 7, 8] }
            qcdScaleSyst = op.systematic(op.c_float(1.), name="qcdScale", **plotter.qcdScaleVariations)
            noSel = noSel.refine("qcdScale", weight=qcdScaleSyst)
        else:
            plotter.qcdScaleVariations = dict()

    if PSISR and "PS" not in buggySyst:
        logger.info("Adding PS ISR systematics")
        if "TTTo" in sample or "TTbb" in sample:
            psISRSyst = op.systematic(op.c_float(1.), name="psISR", up=tree.PSWeight[6], down=tree.PSWeight[8])
        else:
            psISRSyst = op.systematic(op.c_float(1.), name="psISR", up=tree.PSWeight[2], down=tree.PSWeight[0])
        noSel = noSel.refine("psISR", weight=psISRSyst)

    if PSFSR and "PS" not in buggySyst:
        logger.info("Adding PS FSR systematics")
        if "TTTo" in sample or "TTbb" in sample:
            psFSRSyst = op.systematic(op.c_float(1.), name="psFSR", up=tree.PSWeight[7], down=tree.PSWeight[9])
        else:
            psFSRSyst = op.systematic(op.c_float(1.), name="psFSR", up=tree.PSWeight[3], down=tree.PSWeight[1])
        noSel = noSel.refine("psFSR", weight=psFSRSyst)

    return noSel

def splitTTjetFlavours(cfg, tree, noSel):
    subProc = cfg["subprocess"]
    if subProc == "ttbb":
        noSel = noSel.refine(subProc, cut=(tree.genTtbarId % 100) >= 53)
    elif subProc == "ttb":
        noSel = noSel.refine(subProc, cut=op.in_range(50, tree.genTtbarId % 100, 53))
    elif subProc == "ttcc":
        noSel = noSel.refine(subProc, cut=op.in_range(40, tree.genTtbarId % 100, 46))
    elif subProc == "ttjj":
        noSel = noSel.refine(subProc, cut=(tree.genTtbarId % 100) < 41)
    # also alternate splitting with ttB (ttH like)
    elif subProc == "ttB":
        noSel = noSel.refine(subProc, cut=(tree.genTtbarId % 100) >= 51)
    return noSel

def remove4F5FOverlapPlotIt(samplesCfg):
    """Give precedence to 4FS sample if both 5FS and 4FS samples are present for any of the ttbj, ttbb or ttB subprocesses before running plotIt"""
    toRemove = []
    for smpName,smpCfg in samplesCfg.items():
        if not smpCfg.get("is_signal", False):
            continue
        dec = smpName.split("_")[0] # decay channel
        proc = smpCfg.get("subprocess", "")
        syst = smpCfg.get("syst", [None])[0]
        if dec in ["TTToSemiLeptonic", "TTTo2L2Nu", "TTToHadronic"] and proc in ["ttbj", "ttbb", "ttB"]:
            if any((cfg.get("subprocess", "") == proc and nm.startswith(f"TTbb_4f_{dec}_TuneCP5") and cfg.get("syst", [None])[0] == syst) for nm,cfg in samplesCfg.items()):
                toRemove.append(smpName)
    if toRemove:
        logger.info(f"Will remove the following from plotIt list because of 4FS/5FS overlap: {toRemove}")
    for nm in toRemove:
        samplesCfg.pop(nm)

def normalizeAndSumSamples(eras, samples, inDir, outPath):
    """
    Produce file containing the sum of all the histograms over the processes, 
    after normalizing the processes by their cross section, sum of weights and luminosity.
    Note: The systematics are handled but are expected to be SAME for all processes and eras.
    A separate output file is produced for each era (`outPath_era.root`), as well as a total one (`outPath_run2.root`).
    """
    for era in eras:
        lumi = eras[era]["luminosity"]
        mergedHists = {}
        for proc,cfg in samples.items():
            if cfg["era"] != era: continue
            if "syst" in cfg: continue
            sumWgt = cfg["generated-events"]
            xs = cfg["cross-section"]
            tf = HT.openFileAndGet(os.path.join(inDir, proc + ".root"))
            keyList = tf.GetListOfKeys()
            for key in keyList:
                hist = key.ReadObj()
                if not hist.InheritsFrom("TH1"): continue
                hist.Scale(lumi * xs / sumWgt)
                name = hist.GetName()
                if name not in mergedHists:
                    mergedHists[name] = hist.Clone()
                    mergedHists[name].SetDirectory(0)
                else:
                    mergedHists[name].Add(hist)
            tf.Close()
        mergedFile = HT.openFileAndGet(outPath + "_" + era + ".root", "recreate")
        for hist in mergedHists.values():
            hist.Write()
        mergedFile.Close()
    os.system("hadd -f " + outPath + "_run2.root " + " ".join([ f"{outPath}_{era}.root" for era in eras ]))

def produceMEScaleEnvelopes(plots, scaleVariations, path):
    if not scaleVariations:
        return

    tf = HT.openFileAndGet(path, "update")
    listOfKeys = [ k.GetName() for k in tf.GetListOfKeys() ]

    for plot in plots:
        # Compute envelope histograms for QCD scale variations
        nominal = tf.Get(plot.name)
        variations = []
        for var in scaleVariations:
            varName = "{}__{}".format(plot.name, var)
            if varName in listOfKeys:
                variations.append(tf.Get(varName))
        if len(variations) != len(scaleVariations):
            logger.warning("Did not find {} variations for plot {} in file {}".format(len(scaleVariations), plot.name, path))
            continue
        if not variations:
            continue
        up,down = HT.getEnvelopeHistograms(nominal, variations)
        up.Write(f"{plot.name}__qcdScaleup", ROOT.TObject.kOverwrite)
        down.Write(f"{plot.name}__qcdScaledown", ROOT.TObject.kOverwrite)

    tf.Close()

def postProcSystSamples(taskList, samples, resultsdir):
    """
    Rename the systematics samples so they can be interpreted by plotIt
    Also fix the normalization of their histograms
    Finally, remove them from the config sample list for plotIt
    """
    for task in taskList:
        if not "syst" in task.config: continue
        nominalSample = task.config["syst"][1]
        systVar = task.config["syst"][0]

        if nominalSample not in samples:
            logger.warning(f"For syst {systVar} could not find nominal sample {nominalSample}, skipping renormalization step")
            continue

        tfSyst = HT.openFileAndGet(os.path.join(resultsdir, task.outputFile))
        tfNom = HT.openFileAndGet(os.path.join(resultsdir, nominalSample + ".root"), "update")
        wgtRatio = samples[nominalSample]["generated-events"] / task.config["generated-events"]
        for k in tfSyst.GetListOfKeys():
            hist = k.ReadObj()
            if not hist.InheritsFrom("TH1"): continue
            name = hist.GetName() + "__" + systVar
            hist.SetName(name)
            hist.Scale(wgtRatio)
            hist.Write("", ROOT.TObject.kOverwrite)
        tfNom.cd()
        tfNom.Close()
        tfSyst.Close()

        # always remove from list propagated to plotIt since otherwise plotIt counts it as another process
        samples.pop(task.name)

def insertSampleFilesIntoTemplate(template, jsonFiles, selEras=None):
    # Load all JSON files
    jsonSamples = []
    for path in jsonFiles:
        with open(path) as _f:
            jsonSamples.append(json.load(_f))

    outTemplate = {}

    def match(name, sample, era, postCrb):
        if selEras and era not in selEras:
            return False
        regex = sample.get("regex", name + r"(_ext.)?") + "__" + era
        if re.match(regex, postCrb["request_name"]):
            if "syst" in sample:
                syst,nom = sample["syst"]
                newName = f"{nom}__{era}__{syst}"
            else:
                newName = f"{name}__{era}"
            sumw = postCrb["sumw"]
            files = [ PFN_PREFIX + f['lfn'] for f in postCrb['file_infos'] ]
            if newName in outTemplate:
                outTemplate[newName]["generated-events"] += sumw
                outTemplate[newName]["files"] += files
            else:
                outTemplate[newName] = deepcopy(sample)
                outTemplate[newName]["generated-events"] = sumw
                outTemplate[newName]["files"] = files
                outTemplate[newName]["era"] = era
                if "eras" in outTemplate[newName]:
                    outTemplate[newName].pop("eras")
                if "syst" in outTemplate[newName]:
                    outTemplate[newName]["syst"][1] += "__" + era
            logger.debug(f"Adding files of {postCrb['request_name']} to sample {name} for {era}")
            return True
        return False

    # Fill template with file infos
    for name, sample in template.items():
        for postCrb in jsonSamples[:]:
            if "eras" in sample:
                for era in sample["eras"]:
                    if match(name, sample, era, postCrb):
                        postCrb["files-inserted"] = True
            else:
                if match(name, sample, sample["era"], postCrb):
                    postCrb["files-inserted"] = True

    for postCrb in jsonSamples:
        if not postCrb.get("files-inserted", False):
            logger.debug("Sample for request {} and dataset {} has not been recognised!".format(postCrb['request_name'], postCrb['dataset']))

    for name, sample in outTemplate.items():
        if len(sample.get("files", [])) == 0:
            logger.warning("Sample in template {} has no matching post-crab output!".format(name))

    # Expand split processes
    for name, sample in list(outTemplate.items()):
        if "subprocesses" in sample:
            for subProc in sample["subprocesses"]:
                newProc = deepcopy(sample)
                newProc.pop("subprocesses")
                newProc["subprocess"] = subProc
                newProc["group"] = subProc
                # be careful with the systmatics samples (they have two '__' separators)
                newName = name.split("__")[0] + "_" + subProc + "__" + "__".join(name.split("__")[1:])
                if "syst" in newProc:
                    newProc["syst"][1] = newProc["syst"][1].split("__")[0] + "_" + subProc + "__" + newProc["syst"][1].split("__")[1]
                outTemplate[newName] = newProc
            del outTemplate[name]

    return outTemplate
