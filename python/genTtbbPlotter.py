import os

import logging
logger = logging.getLogger("genTtbb plotter")

from bamboo.analysismodules import NanoAODHistoModule, NanoAODSkimmerModule

from bamboo.analysisutils import configureJets, makePileupWeight

from bamboo.plots import Plot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo import treefunctions as op

import definitions as defs
import utils
import controlPlotDefinition as cp
import genDefinitions as genDefs
from genBaseTtbbPlotter import genBaseTtbbPlotter

class genTtbbPlotter(genBaseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)

    def customizeAnalysisCfg(self, analysisCfg):
        super().customizeAnalysisCfg(analysisCfg)

        samples = analysisCfg["samples"]
        for smpNm in list(samples.keys()):
            # remove everything non-ttB
            if samples[smpNm].get("subprocess", "") != "ttB":
                samples.pop(smpNm)
            elif "SemiLeptonic" not in smpNm:
                samples.pop(smpNm)
            else:
                smpCfg = samples[smpNm]
                smpCfg["type"] = "signal"
                if "4f" in smpNm:
                    smpCfg["group"] = "ttbb_4fs"
                else:
                    smpCfg["group"] = "tt_5fs"

    def definePlots(s, t, noSel, sample=None, sampleCfg=None):
        super().defineObjects(t, noSel, sample, sampleCfg)
        era = sampleCfg["era"]
        plots = []

        ##### Muon and electron selection (exclusive!), merged
        genOneLepSel = genBaseTtbbPlotter.defineBaseSelections(s, t, noSel, sample, sampleCfg)

        # number of jets
        genOneLep5JetSel = genOneLepSel.refine("lepton_5jets", cut=op.rng_len(s.genCleanedJets) >= 5)
        genOneLep6JetSel = genOneLepSel.refine("lepton_6jets", cut=op.rng_len(s.genCleanedJets) >= 6)
        genOneLep7JetSel = genOneLepSel.refine("lepton_7jets", cut=op.rng_len(s.genCleanedJets) >= 7)
        # b tags
        genOneLep5Jet3BSel = genOneLep5JetSel.refine("lepton_5jets_3b", cut=op.rng_len(s.genBJets) >= 3)
        genOneLep6Jet3B3LSel = genOneLep6JetSel.refine("lepton_6jets_3b_3l", cut=op.AND(op.rng_len(s.genBJets) >= 3, op.rng_len(s.genLightJets) >= 3))
        genOneLep6Jet4BSel = genOneLep6JetSel.refine("lepton_6jets_4b", cut=op.rng_len(s.genBJets) >= 4)
        genOneLep7Jet4B3LSel = genOneLep7JetSel.refine("lepton_7jets_4b_3l", cut=op.AND(op.rng_len(s.genBJets) >= 4, op.rng_len(s.genLightJets) >= 3))

        # number of b jets
        plots.append(Plot.make1D("1lep_5j_3b_nBJets", op.rng_len(s.genBJets), genOneLep5Jet3BSel,
                EqBin(4, 3, 7), title="Number of b jets", plotopts=utils.getOpts("gen_1lep_5j_3b")))

        for selN,sel,nJ in [("gen_1lep_5j_3b", genOneLep5Jet3BSel, (5,3)), ("gen_1lep_6j_4b", genOneLep6Jet4BSel, (6,4))]:
            plots.append(Plot.make1D(f"{selN}_nJets", op.rng_len(s.genCleanedJets), sel,
                    EqBin(13-nJ[0], nJ[0], 13), title="Number of jets", plotopts=utils.getOpts(selN)))
            for i in range(nJ[1]):
                plots.append(Plot.make1D(f"{selN}_bjet{i+1}_pt", s.genBJets[i].pt, sel,
                        EqBin(60, 20., 720. - min(i, 2)*200), title=f"{utils.getCounter(i+1)} gen b jet p_{{T}} (GeV)",
                        plotopts=utils.getOpts(selN)))
                plots.append(Plot.make1D(f"{selN}_jet{i+1}_eta", s.genBJets[i].eta, sel,
                        EqBin(50, -2.4, 2.4), title=f"{utils.getCounter(i+1)} gen b jet eta",
                        plotopts=utils.getOpts(selN, **{"log-y": False})))

        # closest b-jet pair
        genBJetPairs = op.combine(s.genBJets, N=2)
        genMinDRbbPair = op.rng_min_element_by(genBJetPairs, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        plots += cp.makeExtraJetPlots(genOneLep6Jet4BSel, genMinDRbbPair, "gen_1lep_6j_4b", binScaling=1, pTthresh=20)

        # Largest Mbb pair
        genMaxMbbPair = op.rng_max_element_by(genBJetPairs, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))
        plots.append(Plot.make1D("gen_1lep_6j_4b_largest_Mbb", op.invariant_mass(genMaxMbbPair[0].p4, genMaxMbbPair[1].p4), 
            genOneLep6Jet4BSel, EqBin(60, 60., 1560.), title="Largest Mbb", plotopts=utils.getOpts("gen_1lep_6j_4b")))
        
        # average DRbb
        plots.append(Plot.make1D("gen_1lep_6j_4b_average_DRbb", op.rng_mean(op.map(genBJetPairs, lambda p: op.deltaR(p[0].p4, p[1].p4))), 
            genOneLep6Jet4BSel, EqBin(60, 0.6, 4.), title="Average #Delta R(bb)", plotopts=utils.getOpts("gen_1lep_6j_4b")))

        # Extra light jets -> reconstruct hadronic W
        matchingChi2 = lambda w: op.pow((op.invariant_mass(w) - 79.6) / 4.4, 2.)
        lightJetPairs = op.combine(s.genLightJets, N=2)
        wCand = op.rng_min_element_by(lightJetPairs, lambda pair: matchingChi2(pair[0].p4 + pair[1].p4))
        lightJets_notW = op.select(s.genLightJets, lambda j: op.NOT(op.OR(j == wCand[0], j == wCand[1])))

        for selN,sel in [("gen_1lep_5j_3b_3l", genOneLep6Jet3B3LSel), ("gen_1lep_7j_4b_3l", genOneLep7Jet4B3LSel)]:
            plots.append(Plot.make1D(f"{selN}_wMatch_chi2", matchingChi2(wCand[0].p4 + wCand[1].p4), sel,
                    EqBin(60, 0., 10.), title="W matching chi2", plotopts=utils.getOpts(selN)))
            plots.append(Plot.make1D(f"{selN}_wCand_Mjj", op.invariant_mass(wCand[0].p4 + wCand[1].p4), sel,
                    EqBin(60, 20., 260.), title="W candidate mass (GeV)", plotopts=utils.getOpts(selN)))
            plots.append(Plot.make1D(f"{selN}_extraLightJet_pt", lightJets_notW[0].pt, sel,
                    EqBin(60, 20., 520.), title="Add. light jet p_{T} (GeV)", plotopts=utils.getOpts(selN)))
            plots.append(Plot.make1D(f"{selN}_extraLightJet_eta", lightJets_notW[0].eta, sel,
                    EqBin(50, -2.4, 2.4), title="Add. light jet eta", plotopts=utils.getOpts(selN, **{"log-y": False})))

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super().postProcess(taskList, config, workdir, resultsdir, remove4F5Foverlap=False)
        
        # run plotIt to have the 4f/5f comparison
        self.runPlotIt(taskList, config, workdir, resultsdir)
