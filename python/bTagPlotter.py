import os
import copy

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo import treefunctions as op
from bamboo.root import gbl as ROOT

import definitions as defs
import utils
import controlPlotDefinition as cp
import HistogramTools as HT
from simplePlotter import plotFromConfig

from recoBaseTtbbPlotter import recoBaseTtbbPlotter

class bTagPlotter(recoBaseTtbbPlotter):
    """
    Produce and check b-tagging related stuff.
    First run with either "--effs" or "--patches" to produce b-tagging efficiency maps or 3D patches.
    Then, update the paths pointing to those efficiencies and patches, and rerun again with "--checks" to produce the validation plots.
    """
    def __init__(self, args):
        # make sure the recoBaseTtbbPlotter does not apply any b-tag correction itself
        args += ["--sf-patches", "none"]
        super().__init__(args)

    def addArgs(self, parser):
        super().addArgs(parser)
        parser.add_argument("--effs", action="store_true", help="Produce b-tagging efficiency maps")
        parser.add_argument("--patches", action="store_true", help="Produce SF patches")
        parser.add_argument("--check", action="store_true", help="Produce comparison plots")

    def customizeAnalysisCfg(self, analysisCfg):
        super().customizeAnalysisCfg(analysisCfg)
        samples = analysisCfg["samples"]
        
        for smpNm in list(samples.keys()):
            # make sure there's no data
            if not self.isMC(smpNm):
                samples.pop(smpNm)
            # only run on ttbar/ttbb
            elif not samples[smpNm].get("is_signal", False):
                samples.pop(smpNm)
        
        # remove 4FS/5FS overlap
        utils.remove4F5FOverlapPlotIt(samples)

    def definePlots(s, t, noSel, sample=None, sampleCfg=None):
        if (s.args.effs or s.args.patches) and s.args.check:
            raise RuntimeError("Can either produce efficiencies and patches OR make the check plots; see the --help")
        if not (s.args.effs or s.args.patches or s.args.check):
            raise RuntimeError("Will do nothing! see the --help")

        super().defineObjects(t, noSel, sample, sampleCfg)
        s.selId = 0
        def mkSel(sel, cut, wgt=None):
            s.selId += 1
            return sel.refine(sel.name + str(s.selId), cut, wgt)
        
        era = sampleCfg["era"]
        plots = []

        ##### Muon and electron selection, exclusive!
        oneMuTriggerSel, oneEleTriggerSel = recoBaseTtbbPlotter.defineBaseSelections(s, t, noSel, sample, sampleCfg)

        # shape correction
        bTagWeight = [ op.rng_product(s.cleanedJets, lambda jet: jet.btagSF_deepjet_shape) ]

        oneMu_BWgtAll_Sel = oneMuTriggerSel.refine("1mu_bWgt", weight=bTagWeight)
        oneEle_BWgtAll_Sel = oneEleTriggerSel.refine("1ele_bWgt", weight=bTagWeight)

        #### 3D SF patches
        if s.args.patches:
            for vari, sels in [
                ("", (oneMuTriggerSel, oneEleTriggerSel)), ("_shape", (oneMu_BWgtAll_Sel, oneEle_BWgtAll_Sel)),
            ]:
                htBinning = VarBin([0, 60, 120, 180, 240, 300, 400, 500, 600, 700, 800, 1000, 1500])
                plots += utils.makeMergedPlots([(f"1mu{vari}", sels[0]), (f"1ele{vari}", sels[1])], f"1lep{vari}", "nJets_nBFlav_HT",
                                               (EqBin(10, 0, 10), EqBin(5, 0, 5), htBinning),
                                               (op.rng_len(s.cleanedJets), op.rng_len(s.bFlavJets), s.HT),
                                               plotopts=utils.getOpts("lep"), title="Number of jets vs. number of true b-flavour jets vs. jet HT", xTitle="nJets", yTitle="nBFlav", zTitle="HT")

        #### b-tag efficiencies, for events with >= 4 jets
        fourJetCut = op.rng_len(s.cleanedJets) >= 4
        oneMu_4Jet_Sel = oneMuTriggerSel.refine("muon_4jets", cut=fourJetCut)
        oneEle_4Jet_Sel = oneEleTriggerSel.refine("ele_4jets", cut=fourJetCut)

        if s.args.effs:
            for flav, flavJets in zip(['b', 'c', 'light'], [s.bFlavJets, s.cFlavJets, s.lFlavJets]):
                # b tagging efficiencies as a function of flavour/pt/|eta|
                binning = (VarBin([30, 40, 60, 80, 100, 200, 350, 1000]), EqBin(5, 0, 2.5))

                pt = op.map(flavJets, lambda j: j.pt)
                eta = op.map(flavJets, lambda j: op.abs(j.eta))
                plots.append(Plot.make2D(f"1mu_4j_jet_pt_eta_{flav}", (pt, eta), oneMu_4Jet_Sel, binning))
                plots.append(Plot.make2D(f"1ele_4j_jet_pt_eta_{flav}", (pt, eta), oneEle_4Jet_Sel, binning))
                plots.append(SummedPlot(f"1lep_4j_jet_pt_eta_{flav}", plots[-2:-1]))

                for wp in ["L", "M", "T"]:
                    deepThr = defs.bTagWorkingPoints[era]["btagDeepFlavB"][wp]
                    selJets = op.select(flavJets, lambda j: j.btagDeepFlavB >= deepThr)
                    pt = op.map(selJets, lambda j: j.pt)
                    eta = op.map(selJets, lambda j: op.abs(j.eta))
                    plots.append(Plot.make2D(f"1mu_4j_jet_pt_eta_{flav}_wp{wp}", (pt, eta), oneMu_4Jet_Sel, binning))
                    plots.append(Plot.make2D(f"1ele_4j_jet_pt_eta_{flav}_wp{wp}", (pt, eta), oneEle_4Jet_Sel, binning))
                    plots.append(SummedPlot(f"1lep_4j_jet_pt_eta_{flav}_wp{wp}", plots[-2:-1]))

        if not s.args.check:
            return plots

        ## b-tag efficiency vs. pT for various nJet bins
        pt_binning = VarBin([30, 40, 50, 60, 80, 100, 150, 200, 300, 500, 1000])
        # first for >=4j (same as above)
        for flav, flavJets in zip(['b', 'c', 'light', 'quark', 'gluon'], [s.bFlavJets, s.cFlavJets, s.lFlavJets, s.quarkJets, s.gluonJets]):
            pt = op.map(flavJets, lambda j: j.pt)
            plots.append(Plot.make1D(f"1mu_4j_jet_pt_{flav}", pt, oneMu_4Jet_Sel, pt_binning))
            plots.append(Plot.make1D(f"1ele_4j_jet_pt_{flav}", pt, oneEle_4Jet_Sel, pt_binning))
            plots.append(SummedPlot(f"1lep_4j_jet_pt_{flav}", plots[-2:-1]))
            for tagger,taggerN in [("btagDeepB", "deepCSV"), ("btagDeepFlavB", "deepFlav")]:
                for wp in ["L", "M", "T"]:
                    thr = defs.bTagWorkingPoints[era][tagger][wp]
                    selJets = op.select(flavJets, lambda j: getattr(j, tagger) >= thr)
                    pt = op.map(selJets, lambda j: j.pt)
                    plots.append(Plot.make1D(f"1mu_4j_jet_pt_{flav}_{taggerN}_wp{wp}", pt, oneMu_4Jet_Sel, pt_binning))
                    plots.append(Plot.make1D(f"1ele_4j_jet_pt_{flav}_{taggerN}_wp{wp}", pt, oneEle_4Jet_Sel, pt_binning))
                    plots.append(SummedPlot(f"1lep_4j_jet_pt_{flav}_{taggerN}_wp{wp}", plots[-2:-1]))
        # then for strict nJet bins
        for nJets in range(2, 7):
            if nJets < 6:
                muSel = mkSel(oneMuTriggerSel, cut=op.rng_len(s.cleanedJets) == nJets)
                eleSel = mkSel(oneEleTriggerSel, cut=op.rng_len(s.cleanedJets) == nJets)
            else:
                muSel = mkSel(oneMuTriggerSel, cut=op.rng_len(s.cleanedJets) >= nJets)
                eleSel = mkSel(oneEleTriggerSel, cut=op.rng_len(s.cleanedJets) >= nJets)
            for flav, flavJets in zip(['b', 'c', 'light', 'quark', 'gluon'], [s.bFlavJets, s.cFlavJets, s.lFlavJets, s.quarkJets, s.gluonJets]):
                pt = op.map(flavJets, lambda j: j.pt)
                plots.append(Plot.make1D(f"1mu_eq{nJets}j_jet_pt_{flav}", pt, muSel, pt_binning))
                plots.append(Plot.make1D(f"1ele_eq{nJets}j_jet_pt_{flav}", pt, eleSel, pt_binning))
                plots.append(SummedPlot(f"1lep_eq{nJets}j_jet_pt_{flav}", plots[-2:-1]))
                for tagger,taggerN in [("btagDeepB", "deepCSV"), ("btagDeepFlavB", "deepFlav")]:
                    for wp in ["L", "M", "T"]:
                        thr = defs.bTagWorkingPoints[era][tagger][wp]
                        selJets = op.select(flavJets, lambda j: getattr(j, tagger) >= thr)
                        pt = op.map(selJets, lambda j: j.pt)
                        plots.append(Plot.make1D(f"1mu_eq{nJets}j_jet_pt_{flav}_{taggerN}_wp{wp}", pt, muSel, pt_binning))
                        plots.append(Plot.make1D(f"1ele_eq{nJets}j_jet_pt_{flav}_{taggerN}_wp{wp}", pt, eleSel, pt_binning))
                        plots.append(SummedPlot(f"1lep_eq{nJets}j_jet_pt_{flav}_{taggerN}_wp{wp}", plots[-2:-1]))

        bTagEff = op.define("BTagEffEvaluator", 'const auto <<name>> = BTagEffEvaluator("/t3home/swertz/swertz/bambooOutput/201009_bTag_patch_eff/results/summedProcesses_{era}_ratios.root", {{ {wp} }});'.format(era=era, wp=defs.bTagWorkingPoints[era]["btagDeepFlavB"]["M"]))
        nJetNBHTRwgt = op.define("HistogramEvaluator<double,double,double>", f'const auto <<name>> = HistogramEvaluator<double,double,double>("/t3home/swertz/swertz/bambooOutput/201009_bTag_patch_eff/results/summedProcesses_{era}_ratios.root", "1lep_nJets_nBFlav_HT_sfCorr");')
        nJetHTttHRwgt = defs.getNJetHTttHPatches(sample, sampleCfg, era)

        # fixed working point SFs
        bTagSFPerJet = op.map(s.cleanedJets, lambda j: bTagEff.evaluate(j.hadronFlavour, j.btagDeepFlavB, j.pt, op.abs(j.eta), j.btagSF_deepjet_M))
        bTagWeightFixWP = op.rng_product(bTagSFPerJet)

        # correction weights
        nJetNBHTCorrWgt = nJetNBHTRwgt.evaluate(op.min(op.rng_len(s.cleanedJets), op.static_cast("unsigned long", op.c_int(8))), op.min(op.rng_len(s.bFlavJets), op.static_cast("unsigned long", op.c_int(4))), op.min(s.HT, op.static_cast("float", op.c_float(1000.))))
        nJetHTttHCorrWgt = nJetHTttHRwgt.evaluate(op.min(s.HT, op.static_cast("float", op.c_float(1000.))), op.min(op.rng_len(s.cleanedJets), op.static_cast("unsigned long", op.c_int(8))))
        oneMu_BWgtFixWP_Sel = oneMuTriggerSel.refine("1mu_bWgt_fixWP", weight=bTagWeightFixWP)
        oneEle_BWgtFixWP_Sel = oneEleTriggerSel.refine("1ele_bWgt_fixWP", weight=bTagWeightFixWP)
        oneMu_BWgtAll_nJetNBHTCorr_Sel = oneMu_BWgtAll_Sel.refine("1mu_bWgt_nJetNBHTCorr", weight=nJetNBHTCorrWgt)
        oneEle_BWgtAll_nJetNBHTCorr_Sel = oneEle_BWgtAll_Sel.refine("1ele_bWgt_nJetNBHTCorr", weight=nJetNBHTCorrWgt)
        oneMu_BWgtAll_nJetHTttHCorr_Sel = oneMu_BWgtAll_Sel.refine("1mu_bWgt_nJetHTttHCorr", weight=nJetHTttHCorrWgt)
        oneEle_BWgtAll_nJetHTttHCorr_Sel = oneEle_BWgtAll_Sel.refine("1ele_bWgt_nJetHTttHCorr", weight=nJetHTttHCorrWgt)

        ## overall jet multiplicity and HT
        for vari, sel in [
            ("", (oneMuTriggerSel, oneEleTriggerSel)), ("_shape", (oneMu_BWgtAll_Sel, oneEle_BWgtAll_Sel)),
            ("_fixWP", (oneMu_BWgtFixWP_Sel, oneEle_BWgtFixWP_Sel)),
            ("_shape_nJetNBHTCorr", (oneMu_BWgtAll_nJetNBHTCorr_Sel, oneEle_BWgtAll_nJetNBHTCorr_Sel)),
            ("_shape_nJetHTttHCorr", (oneMu_BWgtAll_nJetHTttHCorr_Sel, oneEle_BWgtAll_nJetHTttHCorr_Sel)),
        ]:
            plots += utils.makeMergedPlots([(f"1mu{vari}", sel[0]), (f"1ele{vari}", sel[1])],
                        f"1lep{vari}", "nJets", EqBin(10, 0, 10), var=op.rng_len(s.cleanedJets),
                        title="Number of jets")
            plots += utils.makeMergedPlots([(f"1mu{vari}", sel[0]), (f"1ele{vari}", sel[1])],
                        f"1lep{vari}", "HT", EqBin(50, 0, 2000), var=s.HT,
                        title="HT")

        #### >= 6 jets
        #### event-level minimum deltaR between jets/b jets/light jets
        jetPairs = op.combine(s.cleanedJets, N=2)
        minJetDR = op.rng_min(jetPairs, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))
        bFlavJetPairs = op.combine(s.bFlavJets, N=2)
        minDRBFlavJetPair = op.rng_min_element_by(bFlavJetPairs, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))
        lightJetPairs = op.combine(s.lFlavJets, N=2)
        minLightJetDR = op.rng_min(lightJetPairs, lambda pair: op.deltaR(pair[0].p4, pair[1].p4))

        bJetPairs = op.combine(s.bJetsM, N=2)
        minDRBJetPair = op.rng_min_element_by(bJetPairs, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))

        for flav,baseSel in [("mu", oneMuTriggerSel), ("ele", oneEleTriggerSel)]:

            oneLep_6Jet_Sel = baseSel.refine(f"{flav}_6jets", cut=op.rng_len(s.cleanedJets) >= 6)
            oneLep_6Jet_BWgtAll_Sel = baseSel.refine(f"{flav}_6jets_bWgt", cut=op.rng_len(s.cleanedJets) >= 6, weight=bTagWeight)
            oneLep_6Jet_BWgtFixWP_Sel = baseSel.refine(f"{flav}_6jets_fixWP", cut=op.rng_len(s.cleanedJets) >= 6, weight=bTagWeightFixWP)
            oneLep_6Jet_BWgtAll_nJetNBHTCorr_Sel = oneLep_6Jet_BWgtAll_Sel.refine(f"{flav}_6jets_bWgt_nJetNBHTCorr", weight=nJetNBHTCorrWgt)
            oneLep_6Jet_BWgtAll_nJetHTttHCorr_Sel = oneLep_6Jet_BWgtAll_Sel.refine(f"{flav}_6jets_bWgt_nJetHTttHCorr", weight=nJetHTttHCorrWgt)

            for vari, sel in [
                ("", oneLep_6Jet_Sel), ("_shape", oneLep_6Jet_BWgtAll_Sel),
                ("_fixWP", oneLep_6Jet_BWgtFixWP_Sel),
                ("_shape_nJetNBHTCorr", oneLep_6Jet_BWgtAll_nJetNBHTCorr_Sel),
                ("_shape_nJetHTttHCorr", oneLep_6Jet_BWgtAll_nJetHTttHCorr_Sel),
            ]:
                plots.append(Plot.make1D(f"1{flav}_6j{vari}_minJetDR", minJetDR, sel, EqBin(40, 0.4, 2.), plotopts=utils.getOpts(f"1{flav}_6j")))
                plots.append(Plot.make1D(f"1{flav}_6j{vari}_minLightJetDR", minLightJetDR, sel, EqBin(40, 0.4, 2.), plotopts=utils.getOpts(f"1{flav}_6j")))

                plots += cp.makeJetPlots(sel, s.cleanedJets, f"1{flav}_6j{vari}", maxJet=6, binScaling=2, allJets=True)
                plots += cp.makeBJetPlots(sel, s.cleanedJets, f"1{flav}_6j{vari}")
                plots += cp.makeExtraJetPlots(mkSel(sel, op.rng_len(s.bFlavJets) >= 4), minDRBFlavJetPair, f"1{flav}_6j_4trueB{vari}")
                plots += cp.makeExtraJetPlots(mkSel(sel, op.rng_len(s.bJetsM) >= 4), minDRBJetPair, f"1{flav}_6j4b{vari}")
                plots.append(Plot.make1D(f"1{flav}_6j{vari}_nDeepFlavM", op.rng_len(s.bJetsM), sel, EqBin(6, 0, 6), title="Number of deepFlavourM b jets"))

        return plots


    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super().postProcess(taskList, config, workdir, resultsdir)
        erasUsed = list((e for e in config["eras"].keys() if e in self.args.eras[1]) if self.args.eras[1] else config["eras"].keys())

        cache = HT.FileCache()

        # merge all MC processes using their cross sections
        utils.normalizeAndSumSamples(config["eras"], config["samples"], resultsdir, os.path.join(resultsdir, "summedProcesses"))

        # compute ratios -> b-tag SF patches (without SF divided by with SF) and b-tagging efficiencies (after b-tagging divided by before b-tagging)
        # do that for each process separately and for the merged files
        # store the output in separate files (with `_ratios` suffix)
        def getRatio(f, n, d, suffix):
            num = f.Get(n)
            den = f.Get(d)
            ratio = num.Clone(num.GetName() + suffix)
            ratio.Divide(den)
            return ratio

        for proc in list(config["samples"].keys()) + ["summedProcesses_" + suff for suff in ["run2"] + erasUsed]:
            in_tf = HT.openFileAndGet(os.path.join(resultsdir, proc + ".root"), "read")
            out_tf = HT.openFileAndGet(os.path.join(resultsdir, proc + "_ratios.root"), "recreate")

            # compute ratio histogram needed to correct the jet multiplicity
            if self.args.patches:
                getRatio(in_tf, "1lep_nJets_nBFlav_HT", "1lep_shape_nJets_nBFlav_HT", "_sfCorr").Write()

            # compute efficiencies (divide histo after cut by total histo)
            for wp in ["L", "M", "T"]:
                for flav in ["b", "c", "light"]:
                    if self.args.effs:
                        getRatio(in_tf, f"1lep_4j_jet_pt_eta_{flav}_wp{wp}", f"1lep_4j_jet_pt_eta_{flav}", "_eff").Write()
                if self.args.check:
                    for flav in ["b", "c", "light", "quark", "gluon"]:
                        for tagger in ["deepCSV", "deepFlav"]:
                            getRatio(in_tf, f"1lep_4j_jet_pt_{flav}_{tagger}_wp{wp}", f"1lep_4j_jet_pt_{flav}", "_eff").Write()

                            for nJets in range(2, 7):
                                getRatio(in_tf, f"1lep_eq{nJets}j_jet_pt_{flav}_{tagger}_wp{wp}", f"1lep_eq{nJets}j_jet_pt_{flav}", "_eff").Write()

            in_tf.Close()
            out_tf.Close()

        if self.args.patches:
            # plot the 2D slices of the 3D SF patches
            outDir = os.path.join(resultsdir, "..", "projections")
            os.makedirs(outDir, exist_ok=True)
            for era in erasUsed:
                for name in ["1lep_nJets_nBFlav_HT_sfCorr"]:
                    hist = HT.loadHisto(os.path.join(resultsdir, f"summedProcesses_{era}_ratios.root"), name, cache)

                    hist.SetXTitle(name.split("_")[1])
                    hist.SetYTitle(name.split("_")[2])
                    hist.SetZTitle(name.split("_")[3])
                    for axis in ["x", "y", "z"]:
                        HT.plot2DSlices(hist, outDir, axis, f"{name}_{era}")

        if self.args.effs:
            # plot the 2D efficiency maps
            outDir = os.path.join(resultsdir, "..", "bTagEffs")
            os.makedirs(outDir, exist_ok=True)
            for era in erasUsed:
                for flav in ["b", "c", "light"]:
                    for wp in ["L", "M", "T"]:
                        name = f"1lep_4j_jet_pt_eta_{flav}_wp{wp}"
                        hist = HT.loadHisto(os.path.join(resultsdir, f"summedProcesses_{era}_ratios.root"), name, cache)
                        style = HT.setTDRStyle()
                        style.SetPadRightMargin(0.15)
                        style.SetLabelSize(0.03, "XYZ")
                        style.SetPaintTextFormat(".3f")

                        title = hist.GetTitle()
                        c = ROOT.TCanvas(HT.randomString(), title, 800, 600)
                        pad = ROOT.TPad(HT.randomString(), "", 0, 0, 1, 1)
                        pad.Draw()
                        pad.cd()
                        hist.Draw("colztexte")
                        hist.GetYaxis().SetTitleOffset(1.5)
                        hist.GetYaxis().SetTitle("Jet |#eta|")
                        hist.GetXaxis().SetTitle("Jet p_{T}")
                        pad.SetLogx()
                        c.Print(os.path.join(outDir, f"{era}_{name}.pdf"))

        if self.args.check:
            logger.info("Producing comparison plots")
            # plots: compare without SF, with SF, with SF+patches
            variations = [
                ("shape", "iterativeFit SFs"),
                ("fixWP", "Fixed WP SFs"),
                ("shape_nJetNBHTCorr", "iterativeFit + 3D patch"),
                ("shape_nJetHTttHCorr", "iterativeFit + 2D patch (ttH)")
            ]
            plotCfg = {
                "jet1_eta": { "xTitle": "Leading jet eta", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet1_eta", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet1_eta"} for v,l in variations] },
                "jet1_pt": { "xTitle": "Leading jet pT", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet1_pt", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet1_pt"} for v,l in variations], "logy": True },
                "jet1_deepFlav": { "xTitle": "Leading jet deepFlavour", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet1_deepFlav", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet1_deepFlav"} for v,l in variations] },
                "jet2_pt": { "xTitle": "Subleading jet pT", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet2_pt", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet2_pt"} for v,l in variations], "logy": True },
                "jet4_deepFlav": { "xTitle": "4th jet deepFlavour", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet4_deepFlav", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet4_deepFlav"} for v,l in variations] },
                "jet6_pt": { "xTitle": "6th jet pT", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_jet6_pt", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_jet6_pt"} for v,l in variations], "logy": True },
                "nBDeepFlavM": { "xTitle": "nBJets", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_nDeepFlavM", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_nDeepFlavM"} for v,l in variations], "logy": True },
                "nJets": { "xTitle": "nJets", "cats": ["1lep"], "nominal": "{cat}_nJets", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_nJets"} for v,l in variations], "logy": True },
                "HT": { "xTitle": "HT", "cats": ["1lep"], "nominal": "{cat}_HT", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_HT"} for v,l in variations], "logy": True },

                "minJetDR": { "xTitle": "minDRjj", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_minJetDR", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_minJetDR"} for v,l in variations] },
                "minJetDR_lightJets": { "xTitle": "minDRjj, light jets", "cats": ["1mu_6j"], "title": "#geq 6 jets", "nominal": "{cat}_minLightJetDR", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_minLightJetDR"} for v,l in variations] },
                
                "trueB_minDRbb_DR": { "xTitle": "DR(bb) for min. DR true b jets", "cats": ["1mu_6j_4trueB"], "title": "#geq 6 jets, #geq 4 true b jets", "nominal": "{cat}_extra_jet_DR", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_DR"} for v,l in variations] },
                "trueB_minDRbb_M": { "xTitle": "M(bb) for min. DR true b jets", "cats": ["1mu_6j_4trueB"], "title": "#geq 6 jets, #geq 4 true b jets", "nominal": "{cat}_extra_jet_M", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_M"} for v,l in variations], "logy": True },
                "trueB_minDRbb_pT": { "xTitle": "pT(bb) for min. DR true b jets", "cats": ["1mu_6j_4trueB"], "title": "#geq 6 jets, #geq 4 true b jets", "nominal": "{cat}_extra_jet_pT", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_pT"} for v,l in variations], "logy": True },
                
                "minDRbb_DR": { "xTitle": "DR(bb) for min. DR b jets", "cats": ["1mu_6j4b"], "title": "#geq 6 jets, #geq 4 b jets", "nominal": "{cat}_extra_jet_DR", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_DR"} for v,l in variations] },
                "minDRbb_M": { "xTitle": "M(bb) for min. DR b jets", "cats": ["1mu_6j4b"], "title": "#geq 6 jets, #geq 4 b jets", "nominal": "{cat}_extra_jet_M", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_M"} for v,l in variations], "logy": True },
                "minDRbb_pT": { "xTitle": "pT(bb) for min. DR b jets", "cats": ["1mu_6j4b"], "title": "#geq 6 jets, #geq 4 b jets", "nominal": "{cat}_extra_jet_pT", "vars": [{"leg": l, "hist": f"{{cat}}_{v}_extra_jet_pT"} for v,l in variations], "logy": True },
            }
            # duplicate each entry based on the categories
            for n,baseP in list(plotCfg.items())[:]:
                plotCfg.pop(n)
                for c in baseP["cats"]:
                    p = copy.deepcopy(baseP)
                    nom = p["nominal"].format(cat=c)
                    p["vars"].append({"leg": "No SFs", "hist": nom, "nominal": True})
                    for v in p["vars"]:
                        v["hist"] = v["hist"].format(cat=c)
                    plotCfg[f"{nom}"] = p

            # produce the plots for all the output files and for the merged files
            outDir = os.path.join(resultsdir, "..", "bTagChecks")
            os.makedirs(outDir, exist_ok=True)
            def _plotSample(proc, era):
                thisCfg = {}
                for n,p in plotCfg.items():
                    thisP = copy.deepcopy(p)
                    thisP["title"] = thisP["title"] + f", {era}" if "title" in thisP else era
                    thisP["root"] = resultsdir
                    thisP["file"] = f"{proc}.root"
                    thisCfg[f"{proc}_{n}"] = thisP
                plotFromConfig(thisCfg, outDir, cache)
            # first separate samples
            for smpNm,smp in config["samples"].items():
                if "Hadronic" in smpNm:
                    continue
                _plotSample(smpNm, smp["era"])
            # then the sums
            for era in erasUsed:
                _plotSample("summedProcesses_" + era, era)

            for era in erasUsed:
                proc = f"summedProcesses_{era}"
                for wp in ["L", "M", "T"]:
                    for tagger in ["deepCSV", "deepFlav"]:
                        # compare the b efficiencies for various jet multiplicities
                        for flav in ["b", "c", "light", "quark", "gluon"]:
                            nom = f"1lep_4j_jet_pt_{flav}_{tagger}_wp{wp}_eff"
                            variations = [ (i, f"={i} jets") for i in range(2, 6) ]
                            variations.append((6, "#geq 6 jets"))
                            thisP = {
                                "xTitle": "Jet pT",
                                "yTitle": "Efficiency",
                                "vars": [{"leg": l, "hist": f"1lep_eq{v}j_jet_pt_{flav}_{tagger}_wp{wp}_eff"} for v,l in variations] + [{"leg": "#geq 4 jets", "hist": nom, "nominal": True}],
                                "logx": True,
                                "title": f"{tagger} {wp} eff., {flav} jets, {era}"
                            }
                            thisP["root"] = resultsdir
                            thisP["file"] = f"{proc}_ratios.root"
                            thisCfg = { f"{proc}_{nom}": thisP }
                            plotFromConfig(thisCfg, outDir, cache)
                        # compare the b efficiencies for quark vs. gluon vs. light
                        nom = f"1lep_4j_jet_pt_light_{tagger}_wp{wp}_eff"
                        thisP = {
                            "xTitle": "Jet pT",
                            "yTitle": "Efficiency",
                            "vars": [{"leg": f"{flav} jets", "hist": f"1lep_4j_jet_pt_{flav}_{tagger}_wp{wp}_eff"} for flav in ("quark", "gluon")] + [{"leg": "Light jets", "hist": nom, "nominal": True}],
                            "logx": True,
                            "title": f"{tagger} {wp} eff. (#geq 4 jets), {era}"
                        }
                        thisP["root"] = resultsdir
                        thisP["file"] = f"{proc}_ratios.root"
                        thisCfg = { f"{proc}_{nom}_lightJetComparisons": thisP }
                        plotFromConfig(thisCfg, outDir, cache)

