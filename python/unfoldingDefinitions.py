from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo import treefunctions as op

import definitions as defs
import utils

import numpy as np

jet_pt_bins = {}
jet_pt_ubound = [ 530., 430., 330., 230., 180., 130., 130., 130., 130., 130., ]
jet_pt_bins['reco'] = [ EqBin(30, 30., jet_pt_ubound[jet_i])     for jet_i in range(10)  ]
jet_pt_bins['gen']  = [ VarBin([ 30., 70., 130., 200., 530. ]),
                        VarBin([ 30., 70., 130., 200., 430. ]),
                        VarBin([ 30., 50., 100., 150., 330. ]),
                        VarBin([ 30., 50., 80., 110., 230.  ]),
                        VarBin([ 30., 50., 80., 110., 180.  ]),
                        VarBin([ 30., 50., 80., 100., 130.  ]), ]

jet_eta_bins = {}
jet_eta_bins['reco'] = [ EqBin(25, -2.4, 2.4 )  for jet_i in range(10)   ]
jet_eta_bins['gen']  = [ EqBin( 8, -2.4, 2.4 )  for jet_i in range(10) ]

njet_bins = {}
njet_bins['reco']   = EqBin(6, 4, 10)
njet_bins['gen']    = EqBin(6, 4, 10)

def makeJetPlots(sel, jets, uname, maxJet=4, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    ph_space = 'gen' if sel.name.startswith('genLevel_') else 'reco'

    for i in range(maxJet):
        plots.append(Plot.make1D(f"{dname}_jet{i+1}_pt", jets[i].pt, sel,
                jet_pt_bins[ph_space][i], title=f"{utils.getCounter(i+1)} jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{dname}_jet{i+1}_eta", jets[i].eta, sel,
                jet_eta_bins[ph_space][i], title=f"{utils.getCounter(i+1)} jet eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
        # plots.append(Plot.make1D(f"{dname}_jet{i+1}_phi", jets[i].phi, sel,
        #         EqBin(50 // binScaling, -3.1416, 3.1416), title=f"{utils.getCounter(i+1)} jet phi", plotopts=utils.getOpts(uname, **{"log-y": False})))


    plots.append(Plot.make1D(f"{dname}_Njets", op.rng_len(jets) , sel,
                njet_bins[ph_space],
                title="Njets", plotopts=utils.getOpts(uname) ))
    return plots

def makeJetMigrationPlots(sel, jets, genJets, uname, maxJet=4, binScaling=1, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    migration_tag = "migrationMatrix"

    for i in range(maxJet):
        plots.append(Plot.make2D(f"{migration_tag}_{dname}_jet{i+1}_pt", (jets[i].pt, genJets[i].pt), sel,
                (jet_pt_bins['reco'][i], jet_pt_bins['gen'][i]), 
                title=f"Migration Matrix {utils.getCounter(i+1)} jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make2D(f"{migration_tag}_{dname}_jet{i+1}_eta", (jets[i].eta, genJets[i].eta), sel,
                (jet_eta_bins['reco'][i], jet_eta_bins['gen'][i]), 
                title=f"Migration Matrix {utils.getCounter(i+1)} jet eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
        # plots.append(Plot.make1D(f"{dname}_jet{i+1}_phi", jets[i].phi, sel,
        #         EqBin(50 // binScaling, -3.1416, 3.1416), title=f"{utils.getCounter(i+1)} jet phi", plotopts=utils.getOpts(uname, **{"log-y": False})))


    plots.append(Plot.make2D(f"{migration_tag}_{dname}_Njets", (op.rng_len(jets), op.rng_len(genJets) ), sel,
                (njet_bins['reco'], njet_bins['gen']),
                title="Migration Matrix Njets", plotopts=utils.getOpts(uname) ))
    return plots


n_extra_jet = 2
extra_jet_pt_bins = {}
extra_jet_pt_bins['reco'] = [ EqBin( 20, 30., 630. - (jet_i) * 300. )     for jet_i in range(n_extra_jet) ]
extra_jet_pt_bins['gen' ] = [ VarBin([30., 60., 90., 140., 200., 280., 630.]),
                              VarBin([30., 50., 70.,  90., 140., 330.]) ]  

extra_jet_eta_bins = {}
extra_jet_eta_bins['reco'] = [ EqBin(20, -2.4, 2.4) for jet_i in range(n_extra_jet) ]
extra_jet_eta_bins['gen' ] = [ EqBin(6,  -2.4, 2.4) for jet_i in range(n_extra_jet) ] 

extra_jet_dr_bins = {}
extra_jet_dr_bins['reco'] = EqBin(20, 0.3, 3.14 )  
extra_jet_dr_bins['gen' ] = VarBin([0.3, 0.4, 0.6, 0.7, 0.85, 1.0, 1.25, 3.14 ] )

extra_jet_mass_bins = {}
extra_jet_mass_bins['reco'] = EqBin( 20, 0., 400.)
extra_jet_mass_bins['gen' ] = VarBin([0, 17.5, 40, 52.5, 65, 82.5, 107.5, 400.])

extra_jet_pt_tot_bins = {}
extra_jet_pt_tot_bins['reco'] = EqBin( 20, 0., 600.) 
extra_jet_pt_tot_bins['gen' ] = VarBin([0., 30., 90., 110., 135., 170., 215., 600.])

def makeExtraJetPlots(sel, jets, uname, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    ph_space = 'gen' if sel.name.startswith('genLevel_') else 'reco'

    for i in range(n_extra_jet):
        plots.append(Plot.make1D(f"{dname}_extra_jet{i+1}_pt", jets[i].pt, sel,
                extra_jet_pt_bins[ph_space][i], title=f"{utils.getCounter(i+1)} extra jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make1D(f"{dname}_extra_jet{i+1}_eta", jets[i].eta, sel,
                extra_jet_eta_bins[ph_space][i], title=f"{utils.getCounter(i+1)} extra jet eta",
                plotopts=utils.getOpts(uname, **{"log-y": False})))
        # plots.append(Plot.make1D(f"{dname}_extra_jet{i+1}_phi", jets[i].phi, sel,
        #         EqBin(50, -3.1416, 3.1416), title=f"utils.getCounter(i+1) extra jet phi",
        #         plotopts=utils.getOpts(uname, **{"log-y": False})))
    
    plots.append(Plot.make1D(f"{dname}_extra_jet_DR", op.deltaR(jets[0].p4, jets[1].p4),
            sel, extra_jet_dr_bins[ph_space], title="Extra jets DR",
            plotopts=utils.getOpts(uname, **{"log-y": False})))
    plots.append(Plot.make1D(f"{dname}_extra_jet_M", op.invariant_mass(jets[0].p4, jets[1].p4),
            sel, extra_jet_mass_bins[ph_space],
            title="Extra jets invariant mass (GeV)", plotopts=utils.getOpts(uname)))
    plots.append(Plot.make1D(f"{dname}_extra_jet_pT", (jets[0].p4 + jets[1].p4).Pt(),
            sel, extra_jet_pt_tot_bins[ph_space], title="Extra jet pair p_{T} (GeV)",
            plotopts=utils.getOpts(uname)))
    
    return plots

def makeExtraJetMigrationPlots(sel, jets, genJets, uname, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    migration_tag = 'migrationMatrix'

    for i in range(2):
        plots.append(Plot.make2D(f"{migration_tag}_{dname}_extra_jet{i+1}_pt", (jets[i].pt, genJets[i].pt), sel,
                (extra_jet_pt_bins['reco'][i],extra_jet_pt_bins['gen'][i]), title=f"Migration Matrix {utils.getCounter(i+1)} extra jet p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))
        plots.append(Plot.make2D(f"{migration_tag}_{dname}_extra_jet{i+1}_eta", (jets[i].eta, genJets[i].eta), sel,
                (extra_jet_eta_bins['reco'][i],extra_jet_eta_bins['gen'][i]), title=f"Migration Matrix {utils.getCounter(i+1)} extra jet eta",
                plotopts=utils.getOpts(uname)))
        # plots.append(Plot.make1D(f"{dname}_extra_jet{i+1}_phi", jets[i].phi, sel,
        #         EqBin(50, -3.1416, 3.1416), title=f"utils.getCounter(i+1) extra jet phi",
        #         plotopts=utils.getOpts(uname, **{"log-y": False})))
    
    plots.append(Plot.make2D(f"{migration_tag}_{dname}_extra_jet_DR", (op.deltaR(jets[0].p4, jets[1].p4), op.deltaR(genJets[0].p4, genJets[1].p4)),
            sel, ( extra_jet_dr_bins['reco'],extra_jet_dr_bins['gen']) , title="Migration Matrix Extra jets DR",
            plotopts=utils.getOpts(uname)))
    plots.append(Plot.make2D(f"{migration_tag}_{dname}_extra_jet_M", (op.invariant_mass(jets[0].p4, jets[1].p4), op.invariant_mass(genJets[0].p4, genJets[1].p4) ),
            sel, (extra_jet_mass_bins['reco'],extra_jet_mass_bins['gen']),
            title="Migration Matrix Extra jets invariant mass (GeV)", plotopts=utils.getOpts(uname)))
    plots.append(Plot.make2D(f"{migration_tag}_{dname}_extra_jet_pT", ( (jets[0].p4 + jets[1].p4).Pt(), (genJets[0].p4 + genJets[1].p4).Pt() ),
            sel, (extra_jet_pt_tot_bins['reco'],extra_jet_pt_tot_bins['gen']), title="Migration Matrix Extra jet pair p_{T} (GeV)",
            plotopts=utils.getOpts(uname)))
    
    return plots

max_mbb_bins = {}
max_mbb_bins['reco'] = EqBin(25, 0., 1000.) 
max_mbb_bins['gen']  = VarBin([0., 50., 100., 150., 250., 500., 1000.]) 


def makeMaxMbbPairPlots( sel, maxMbbPair,  uname, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    ph_space = 'gen' if sel.name.startswith('genLevel_') else 'reco'

    plots.append(Plot.make1D(f"{dname}_largest_Mbb", op.invariant_mass(maxMbbPair[0].p4, maxMbbPair[1].p4),
        sel, max_mbb_bins[ph_space], title="Largest Mbb", plotopts=utils.getOpts(uname)))

    return plots

def makeMaxMbbPairMigrationPlots( sel, maxMbbPair, genMaxMbbPair, uname, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    migration_tag = 'migrationMatrix'

    plots.append(Plot.make2D(f"{migration_tag}_{dname}_largest_Mbb", 
        (op.invariant_mass(maxMbbPair[0].p4, maxMbbPair[1].p4), op.invariant_mass(genMaxMbbPair[0].p4, genMaxMbbPair[1].p4) ),
        sel, (max_mbb_bins['reco'], max_mbb_bins['gen']), title="Migration Matrix Largest Mbb", plotopts=utils.getOpts(uname)))

    return plots

bjet_pt_bins = {}
bjet_pt_bins['reco'] = EqBin( 25,  30., 180. )
bjet_pt_bins['gen']  = VarBin( [30., 40., 50., 70., 110., 180.] )

def makeBJetPlots(sel, jets, uname, maxJet=4, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    ph_space = 'gen' if sel.name.startswith('genLevel_') else 'reco'

    if maxJet >= 4:
        plots.append(Plot.make1D(f"{dname}_bjet4_pt", jets[3].pt, sel,
                bjet_pt_bins[ph_space], title=f"b jet4 p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))

    return plots

def makeBJetMigrationPlots(sel, jets, genJets, uname, maxJet=4, obs_delim='_obsis'):
    plots = []
    dname = uname + obs_delim
    migration_tag = 'migrationMatrix'

    if maxJet >= 4:
        plots.append(Plot.make2D(f"{migration_tag}_{dname}_bjet4_pt", (jets[3].pt, genJets[3].pt), sel,
                (bjet_pt_bins['reco'], bjet_pt_bins['gen']), title=f"Migration Matrix b jet4 p_{{T}} (GeV)",
                plotopts=utils.getOpts(uname)))

    return plots


##ljet_pt_bins = {}
##ljet_pt_bins['reco'] = EqBin( 25,  30., 230. )
##ljet_pt_bins['gen']  = VarBin( [30., 50., 80., 120., 230.] )
##
##def makeLightJetPlots(sel, jets, uname, maxJet=4, obs_delim='_obsis'):
##    plots = []
##    dname = uname + obs_delim
##    ph_space = 'gen' if sel.name.startswith('genLevel_') else 'reco'
##
##    if len(jets):
##        plots.append(Plot.make1D(f"{dname}_ljet3_pt", jets[2].pt, sel,
##                ljet_pt_bins[ph_space], title=f"light jet3 p_{{T}} (GeV)",
##                plotopts=utils.getOpts(uname)))
##
##    return plots
##
##def makeLightJetMigrationPlots(sel, jets, genJets, uname, maxJet=4, obs_delim='_obsis'):
##    plots = []
##    dname = uname + obs_delim
##    migration_tag = 'migrationMatrix'
##
##    if maxJet >= 3: 
##        plots.append(Plot.make2D(f"{migration_tag}_{dname}_light_jet3_pt", (jets[2].pt, genJets[2].pt), sel,
##                (ljet_pt_bins['reco'], ljet_pt_bins['gen']), title=f"Migration Matrix light jet3 p_{{T}} (GeV)",
##                plotopts=utils.getOpts(uname)))
    
