import os

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.plots import Plot, SummedPlot
from bamboo.plots import EquidistantBinning as EqBin
from bamboo.plots import VariableBinning as VarBin
from bamboo import treefunctions as op
from bamboo.root import gbl as ROOT

import numpy as np

import definitions as defs
import utils
import controlPlotDefinition as cp
import HistogramTools as HT

import genDefinitions as genDefs
import unfoldingDefinitions as unfDefs

from genBaseTtbbPlotter import genBaseTtbbPlotter
from recoBaseTtbbPlotter import recoBaseTtbbPlotter

class unfoldingPlotter(recoBaseTtbbPlotter, genBaseTtbbPlotter):
    """"""
    def __init__(self, args):
        super().__init__(args)

    def definePlots(self, t, noSel, sample=None, sampleCfg=None):
        super().defineObjects(t, noSel, sample, sampleCfg)
        era = sampleCfg["era"]
        plots = []

        ##### Muon and electron selection, exclusive!
        oneMuTriggerSel, oneEleTriggerSel = recoBaseTtbbPlotter.defineBaseSelections(self, t, noSel, sample, sampleCfg)

        ###### Plots for ==1 lepton, >=4 jets (ttbar level) ######
        sixJetSel = op.rng_len(self.cleanedJets) >= 6
        oneMu6JetSel = oneMuTriggerSel.refine("muon_6jets", cut=sixJetSel)
        oneEle6JetSel = oneEleTriggerSel.refine("ele_6jets", cut=sixJetSel)

        ##### Plots for ==1 lepton, >=6 jets, >= 4 b jets (ttbb level) ######
        bTagDef4 = op.rng_len(self.bJetsM) >= 4
        oneMu6Jet4BSel = oneMu6JetSel.refine("muon_6jets_4b", cut=bTagDef4, weight=self.bTagWeight)
        oneEle6Jet4BSel = oneEle6JetSel.refine("ele_6jets_4b", cut=bTagDef4, weight=self.bTagWeight)

        # Find couple of extra bjets
        bJetCouples = op.combine(self.bJetsM, N=2)
        minDRbbPair = op.rng_min_element_by(bJetCouples, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))
        maxMbbPair  = op.rng_max_element_by(bJetCouples, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))
    
        # For ttbar processes we need plots with only particle-level selection, for acceptance,
        # and plots with reco- and particle-level selection for migration matrices.
        do_migrations = False

        if sampleCfg.get("is_signal", False):

            do_migrations = True

            # Muon and electron selection (exclusive!), merged
            genOnlyOneLepSel = genBaseTtbbPlotter.defineBaseSelections(self, t, noSel, sample, sampleCfg, prefix="genLevel_")

            genJetCut =  op.rng_len(self.genCleanedJets) >= 6 
            genBJetCut =  op.rng_len(self.genBJets) >= 4 

            # number of jets
            genOnlyOneLep6Jet4BSel = genOnlyOneLepSel.refine(f"genLevel_lepton_6j_4b", cut= op.AND(genJetCut, genBJetCut) )

            genBJetCouples = op.combine(self.genBJets, N=2)
            genMinDRbbPair = op.rng_min_element_by(genBJetCouples, lambda bcoup: op.deltaR(bcoup[0].p4, bcoup[1].p4))
            genMaxMbbPair = op.rng_max_element_by(genBJetCouples, lambda pair: op.invariant_mass(pair[0].p4, pair[1].p4))

            genOnlySel = genOnlyOneLep6Jet4BSel
            plots += unfDefs.makeJetPlots(        genOnlySel, self.genCleanedJets, genOnlySel.name, maxJet=6)
            plots += unfDefs.makeExtraJetPlots(   genOnlySel, genMinDRbbPair, genOnlySel.name )
            plots += unfDefs.makeMaxMbbPairPlots( genOnlySel,  genMaxMbbPair, genOnlySel.name )
            plots += unfDefs.makeBJetPlots(       genOnlySel,  self.genBJets, genOnlySel.name )
            #plots += unfDefs.makeLightJetPlots(       genOnlySel,   genLightJets, genOnlySel.name )

        for sel,lep,name in [(oneMu6Jet4BSel, self.muon, "1mu_6j_4b"), (oneEle6Jet4BSel, self.electron, "1ele_6j_4b")]:


            plots += unfDefs.makeJetPlots(sel, self.cleanedJets, name, maxJet=6 )
            plots += unfDefs.makeExtraJetPlots(sel, minDRbbPair, name)
            plots += unfDefs.makeMaxMbbPairPlots( sel, maxMbbPair, name )
            plots += unfDefs.makeBJetPlots( sel, self.bJetsM, name )
            #plots += unfDefs.makeLightJetPlots( sel, self.lightJetsM, name )
        
            if do_migrations: 
                #Gen definitions previously initialized when making acceptance plots

                # Muon and electron selection (exclusive!), merged
                genOneLepSelAfterReco = genBaseTtbbPlotter.defineBaseSelections(self, t, sel, sample, sampleCfg, tag=f'_{sel.name}')

                # b tags
                genOneLep6Jet4BSelAfterReco = genOneLepSelAfterReco.refine(f"lepton_6j_4b_{sel.name}", cut=op.AND(genJetCut, genBJetCut) )
                
                plots += unfDefs.makeJetMigrationPlots( genOneLep6Jet4BSelAfterReco, self.cleanedJets, self.genCleanedJets, name, maxJet=6 )
                plots += unfDefs.makeExtraJetMigrationPlots( genOneLep6Jet4BSelAfterReco, minDRbbPair, genMinDRbbPair, name)
                plots += unfDefs.makeMaxMbbPairMigrationPlots(genOneLep6Jet4BSelAfterReco, maxMbbPair, genMaxMbbPair, name )
                plots += unfDefs.makeBJetMigrationPlots( genOneLep6Jet4BSelAfterReco,   self.bJetsM, self.genBJets,  name )
                #plots += unfDefs.makeLightJetMigrationPlots( genOneLep6Jet4BSelAfterReco,   self.lightJetsM, self.genLightJets,  name )

        return plots

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None):
        super().postProcess(taskList, config, workdir, resultsdir)

        self.runPlotIt(taskList, config, workdir, resultsdir)
