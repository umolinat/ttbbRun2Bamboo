from bamboo import treefunctions as op

#### Object definitions

def muonDef(mu):
    return op.AND(
        mu.pt > 26., op.abs(mu.eta) < 2.4,
        #ID
        op.OR(mu.pdgId == 13, mu.pdgId == -13),
    )

def vetoMuonDef(mu):
    return op.AND(
        mu.pt > 15., op.abs(mu.eta) < 2.4,
        #ID
        op.OR(mu.pdgId == 13, mu.pdgId == -13),
    )

def eleDef(ele):
    return op.AND(
        op.AND(ele.pt > 29, op.abs(ele.eta) < 2.4),
        op.OR(ele.pdgId == 11, ele.pdgId == -11),
    )

def vetoEleDef(ele):
    return op.AND(
        ele.pt > 15., op.abs(ele.eta) < 2.5,
        #ID
        op.OR(ele.pdgId == 11, ele.pdgId == -11),
    )

def jetDef(jet):
    return op.AND(
        jet.pt > 20., op.abs(jet.eta) < 2.4
    )

def cleanJets(jets, muons, electrons):
    return op.select(jets, lambda jet: op.AND(
            op.NOT(op.rng_any(electrons, lambda ele: op.deltaR(jet.p4, ele.p4) < 0.4 )),
            op.NOT(op.rng_any(muons, lambda mu: op.deltaR(jet.p4, mu.p4) < 0.4 ))
        ))

def buildLeptonSelection(noSel, muons, vetoMuons, electrons, vetoElectrons, tag="", prefix=''):
    muonChannel = op.AND(
            op.rng_len(muons) == 1,
            op.rng_len(vetoMuons) == 1,
            op.rng_len(vetoElectrons) == 0
        )
    electronChannel = op.AND(
            op.rng_len(vetoMuons) == 0,
            op.rng_len(vetoElectrons) == 1,
            op.rng_len(electrons) == 1
        )
    # the 2 channels are exclusive, so we can safely combine them in a single selection
    return noSel.refine(f"{prefix}lepton{tag}", cut=op.OR(muonChannel, electronChannel))
