import os
import json
import yaml
import glob
import shutil

import logging
logger = logging.getLogger("ttbb plotter")

from bamboo.analysismodules import NanoAODHistoModule

from bamboo.analysisutils import parseAnalysisConfig
from bamboo.root import addIncludePath, loadHeader

import definitions as defs
import utils

class baseTtbbPlotter(NanoAODHistoModule):
    """ Base class for all plotters, prepares everything needed for both reco-level and gen-level analysis """
    def __init__(self, args):
        super().__init__(args)

        addIncludePath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "..", "include"))
        loadHeader("HistogramEvaluator.h")

        self.plotDefaults = {
            "y-axis"           : "Events",
            "log-y"            : "both",
            "y-axis-show-zero" : True,
            "save-extensions"  : ["pdf"],
            "show-ratio"       : True,
            "sort-by-yields"   : True,
        }

        self.doSysts = self.args.systematic

    def addArgs(self, parser):
        super().addArgs(parser)
        parser.add_argument("--test", action="store_true", help="Test max. 1 MC and 1 data file (to test before launching a large job submission)")
        parser.add_argument("-s", "--systematic", action="store_true", help="Produce systematic variations")
        parser.add_argument("-r", "--reduce-split", type=int, default=0, help="Reduce number of jobs by factor X")
        parser.add_argument("--samples", nargs='*', help="Sample template YML files: JSON files are inserted from env. variable SAMPLE_JSONS")

    def initialize(self):
        super().initialize()
        # If required, insert list of files from runPostCrab JSONS at this stage,
        # write the resulting full analysis yml into the output dir and make sure the worker jobs use that new one.
        # This must be done here to change the analysis cfg that will be used later on directly in `args.inputs[0]`
        if self.args.samples and (not self.args.distributed or self.args.distributed != "worker"):
            analysisCfg = parseAnalysisConfig(self.args.input[0])
            json_folder = os.getenv("SAMPLE_JSONS")
            if not json_folder:
                raise RuntimeError("Should specify a SAMPLE_JSONS environment variable when using the 'samples' argument!")
            jsons = glob.glob(json_folder + "/*.json")
            eras = self.args.eras[1]
            sampleDict = {}
            for tmpPath in self.args.samples:
                with open(tmpPath) as f_:
                    template = yaml.load(f_, Loader=yaml.SafeLoader)
                    sampleDict.update(utils.insertSampleFilesIntoTemplate(template, jsons, eras))
            analysisCfg["samples"] = sampleDict
            newCfg = os.path.join(self.args.output, "full_analysis.yml")
            os.makedirs(self.args.output, exist_ok=True)
            with open(newCfg, "w") as f_:
                yaml.dump(analysisCfg, f_)
            self.args.input = [newCfg]

    def customizeAnalysisCfg(self, analysisCfg):
        samples = analysisCfg["samples"]
        
        # reduce job splitting
        if self.args.reduce_split:
            for smp in samples.values():
                smp["split"] *= self.args.reduce_split
        
        # if we're not doing systematics, remove the systematics samples from the list
        if not self.doSysts:
            for smp in list(samples.keys()):
                if "syst" in samples[smp]:
                    samples.pop(smp)

        if self.args.test and not self.args.distributed:
            # only keep 1 MC (if possible, a signal) and 1 data file of any era, for testing the plotter
            foundMC = None
            foundData = None
            chosenEra = None
            for smpNm,smp in samples.items():
                if not foundData and not self.isMC(smpNm):
                    if chosenEra:
                        if smp["era"] == chosenEra:
                            foundData = smpNm
                    else:
                        foundData = smpNm
                        chosenEra = smp["era"]
                if not foundMC and smp.get("is_signal", False) and self.isMC(smpNm):
                    if chosenEra:
                        if smp["era"] == chosenEra:
                            foundMC = smpNm
                    else:
                        foundMC = smpNm
                        chosenEra = smp["era"]
                if foundMC and foundData:
                    break
            # no signal found, now look for any MC
            if not foundMC:
                for smpNm,smp in samples.items():
                    if self.isMC(smpNm):
                        if chosenEra:
                            if smp["era"] == chosenEra:
                                foundMC = smpNm
                        else:
                            foundMC = smpNm
                            chosenEra = smp["era"]
                        break
            for smpNm in list(samples.keys()):
                if smpNm != foundMC and smpNm != foundData:
                    samples.pop(smpNm)
            # only keep 1 file per sample
            for smpNm,smp in samples.items():
                smp["files"] = [smp["files"][0]]
            # adjust the eras in the analysis config
            for era in list(analysisCfg["eras"].keys()):
                if era != chosenEra:
                    analysisCfg["eras"].pop(era)

    def prepareTree(self, tree, sample=None, sampleCfg=None):
        era = sampleCfg["era"]
        tree,noSel,be,lumiArgs = super().prepareTree(tree, sample=sample, sampleCfg=sampleCfg, description=defs.getNanoAODDescription(era, self.isMC(sample)), lazyBackend=True)

        if self.isMC(sample):
            # if it's a systematics sample, turn off other systematics
            if "syst" in sampleCfg:
                self.doSysts = False

            noSel = noSel.refine("mcWeight", weight=tree.genWeight, autoSyst=self.doSysts)

            if self.doSysts:
                noSel = utils.addTheorySystematics(self, sample, sampleCfg, tree, noSel)

            if "subprocess" in sampleCfg:
                noSel = utils.splitTTjetFlavours(sampleCfg, tree, noSel)

        else: ## DATA
            pass

        return tree,noSel,be,lumiArgs

    def defineObjects(self, tree, noSel, sample=None, sampleCfg=None):
        pass

    def postProcess(self, taskList, config=None, workdir=None, resultsdir=None,
                    makeBU=True, removeBatch=True, createEnvelope=True, moveSystHists=True, removeFileList=True, remove4F5Foverlap=True):
        self.plotList = self.getPlotList(resultsdir=resultsdir)

        if makeBU:
            # create backup directory with all merged results
            # if something screws up in the postprocessing, just copy it back as `results`
            resultsdir_bu = os.path.join(workdir, "results_backup")
            if not os.path.isdir(resultsdir_bu):
                shutil.copytree(resultsdir, resultsdir_bu)

        if removeBatch:
            # remove batch output files (not needed anymore since they've been copied/merged into the results directory)
            batchOut = os.path.join(workdir, "batch", "output")
            if os.path.isdir(batchOut):
                shutil.rmtree(batchOut)

        if createEnvelope:
            # create QCD scale envelopes - if needed (only for 7-point variation, which defines self.qcdScaleVariations)
            for task in taskList:
                if self.doSysts and self.isMC(task.outputFile):
                    utils.produceMEScaleEnvelopes(self.plotList, self.qcdScaleVariations, os.path.join(resultsdir, task.outputFile))

        if moveSystHists:
            # renormalize, copy and rename histograms from systematic variation files into nominal files
            # also remove the systematic samples from the plotIt list
            if self.doSysts:
                utils.postProcSystSamples(taskList, config["samples"], resultsdir)

        if removeFileList:
            # remove the file lists from the yml config - not needed for plotIt
            for smpName,smpCfg in list(config["samples"].items()):
                smpCfg.pop("files")

        if remove4F5Foverlap:
            # remove overlap between 4FS and 5FS ttbb/ttB contributions for plotIt
            utils.remove4F5FOverlapPlotIt(config["samples"])

    def runPlotIt(self, taskList, config=None, workdir=None, resultsdir=None):
        # run plotIt as defined in HistogramsModule
        NanoAODHistoModule.postProcess(self, taskList, config, workdir, resultsdir)
