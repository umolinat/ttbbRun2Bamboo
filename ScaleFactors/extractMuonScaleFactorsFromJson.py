#! /bin/env python

import math
import argparse
import json
import re
import os

parser = argparse.ArgumentParser()
parser.add_argument('file', help='Json file containing muon scale factors')
parser.add_argument('-s', '--suffix', help='Suffix to append at the end of the output filename', required=True)
parser.add_argument('-e', '--era', help='Specify the era', required=True)
parser.add_argument('-m', '--mode', choices=['id', 'iso'], help='Retrieve either ID or isolation SF')

args = parser.parse_args()

with open(args.file) as _f:
    data = json.load(_f)

if args.era == "2016":
    if args.mode == 'id':
        num = "TightID"
        den = "genTracks"
    if args.mode == 'iso':
        num = 'TightRelIso'
        den = 'TightIDandIPCut'
    name = 'pt_eta'
    xVar = 'pt'
    bin_xVar = 'Pt'
    yVar = 'eta'
    bin_yVar = 'Eta'
elif args.era == "2017":
    if args.mode == 'id':
        num = "TightID"
        den = "genTracks"
    if args.mode == 'iso':
        num = 'TightRelIso'
        den = 'TightIDandIPCut'
    name = 'abseta_pt'
    xVar = 'abseta'
    bin_xVar = 'AbsEta'
    yVar = 'pt'
    bin_yVar = 'Pt'
elif args.era == "2018":
    if args.mode == 'id':
        num = "TightID"
        den = "TrackerMuons"
    if args.mode == 'iso':
        num = 'TightRelIso'
        den = 'TightIDandIPCut'
    name = 'abseta_pt'
    xVar = 'abseta'
    bin_xVar = 'AbsEta'
    yVar = 'pt'
    bin_yVar = 'Pt'

entry = "NUM_{}_DEN_{}".format(num, den)
scaleFactors = data[entry][name]

xVarbinning = set()
yVarbinning = set()

def getBoundaries(var, string):
    start = re.search(var + r':\[(.*),.*\]', string)
    stop = re.search(var + r':\[.*,(.*)\]', string)
    return (start.group(1), stop.group(1))

for key in scaleFactors:
    bounds = getBoundaries(xVar, key) 
    xVarbinning.add(bounds[0])
    xVarbinning.add(bounds[1])
xVarbinning = sorted(list(xVarbinning), key=lambda x: float(x))

for key in next(iter(scaleFactors.values())):
    bounds = getBoundaries(yVar, key)
    yVarbinning.add(bounds[0])
    yVarbinning.add(bounds[1])
yVarbinning = sorted(list(yVarbinning), key=lambda x: float(x))

json_content = {'dimension': 2, 'variables': [bin_xVar, bin_yVar], 'binning': {'x': [float(x) for x in xVarbinning], 'y': [float(x) for x in yVarbinning]}, 'data': [], 'error_type': 'absolute'}
json_content_data = json_content['data']

for i in range(0, len(xVarbinning) - 1):
    xVardata = {'bin': [xVarbinning[i], xVarbinning[i + 1]], 'values': []}
    xVarentry = scaleFactors[xVar + ":[{},{}]".format(xVarbinning[i], xVarbinning[i+1])]
    
    for j in range(0, len(yVarbinning) - 1):
        yVarentry = xVarentry[yVar + ":[{},{}]".format(yVarbinning[j], yVarbinning[j+1])]
        value = yVarentry["value"]
        stat = yVarentry["stat"]
        syst = yVarentry["syst"]
        error = math.sqrt(stat**2 + syst**2)
        yVardata = {'bin': [yVarbinning[j], yVarbinning[j + 1]], 'value': value, 'error_low': error, 'error_high': error}
        xVardata['values'].append(yVardata)

    json_content_data.append(xVardata)

# Save JSON file
filename = 'Muon_%s.json' % args.suffix
with open(filename, 'w') as j:
    json.dump(json_content, j, indent=2)
